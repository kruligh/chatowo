<?php
include('utils/headers.php');

require_once('database/UserDbDao.php');
require_once('database/DbConnection.php');
require_once ('model/MailBody.php');
require_once ('class/MailSender.php');
require_once ('class/Validator.php');

include ('utils/post_input.php');

if( empty($input->nick) || empty($input->email) || empty($input->password) || empty($input->city) || empty($input->birthday)|| strlen($input->sex) ===0){
    header("status",true,400);
    echo "INVALID_ARGUMENTS";
    die();
}

Validator::validateNick($input->nick);
Validator::validateCity($input->city);
Validator::validateEmail($input->email);
Validator::validateBirthDay($input->birthday);


$mailer = new MailSender();

$userDao = new UserDbDao();

$result = $userDao->findActiveByNick($input->nick);

if(!empty($result)) {
    header("status",true,400);
    echo "nick jest zajęty";
    die();
}

if($userDao->checkIfEmailExists($input->email)){
    header("status",true,400);
    echo "istnieje użytkownik z takim emailem!";
    die();
}

$conn = DbConnection::getConnection();

$conn->begin_transaction();
$activationToken = md5(rand(0,9999));
$salt = substr(md5(rand(0,9999)),0,5);
$userDao->registerUser($input->nick,$input->email,$input->password, $salt, $input->city, $input->birthday, $input->sex, $activationToken);
try{
    $mailBody = $mailer->createActivationMail($input->email,$input->nick,$activationToken);
}catch (Exception $e){
    header("status",true,400);
    echo $e->getMessage();
    $conn->rollback();
    die();
}

try{
    $mailer->sendMail($mailBody);
}catch (Exception $e){
    header("status",true,400);
    echo $e->getMessage();
    $conn->rollback();
    die();
}

$conn->commit();

echo "OK";
