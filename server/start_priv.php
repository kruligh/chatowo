<?php
include ('utils/headers.php');
include ('database/DbConnection.php');
include ('utils/auth.php');

require_once ('database/UserDbDao.php');
require_once ('database/SubscriptionDao.php');

include('utils/post_input.php');

if(empty($input->userId)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$subscriptionDao = new SubscriptionDao();

$subscriptionDao->createPriv($session->getUserId(),$input->userId);