<?php
include('utils/headers.php');
include('utils/auth_admin.php');
include ('utils/post_input.php');
require_once('database/RoomsDao.php');

if(empty($input->roomId) || empty($input->newRoomName)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$roomDao = new RoomsDao();

$roomDao->editRoomName($input->roomId, $input->newRoomName);
