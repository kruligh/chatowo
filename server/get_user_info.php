<?php
include('utils/headers.php');
include('utils/auth.php');

require_once('database/UserDbDao.php');

$inputId = $_GET['id'];

if(empty($inputId)){
    header('status',true, 400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$userDao = new UserDbDao();

$result = $userDao->getUserInfo($inputId);

$result = json_encode($result);

if(empty($result)){
    header('status',true,400);
    echo 'EMPTY_RESPONSE'.$result;
    die();
}

echo $result;