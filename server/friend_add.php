<?php
include ('utils/headers.php');
include ('utils/auth_user.php');
include ('utils/post_input.php');

require_once ('database/FriendsDao.php');


if(empty($input->userId)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$friendsDao = new FriendsDao();

if($friendsDao->isFriendshipExists($session->getUserId(), $input->userId)){
    header('status',true,409);
    echo 'FRIENDSHIP_EXISTS';
    die();
}
$friendsDao->addFriend($session->getUserId(), $input->userId);
