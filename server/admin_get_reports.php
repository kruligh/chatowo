<?php
include('utils/headers.php');
include('utils/auth_admin.php');
require_once('database/ReportDao.php');

$reportsDao = new ReportDao();

$reports = $reportsDao->getNewReports();

echo json_encode($reports);