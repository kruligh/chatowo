<?php
include ('auth.php');

require_once(dirname(__DIR__).'/database/UserDbDao.php');

$userDao = new UserDbDao();

$user = $userDao->getUserInfo($session->getUserId());

if($user->getUserType() != 'ADMIN'){
    header("status",true,401);
    echo 'ADMIN ONLY';
    die();
}
