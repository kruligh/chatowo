<?php
//header("Access-Control-Allow-Origin: http://192.168.1.2");
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, X-Auth-Token, last-hit");

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header("status",true,200);
    die();
}

include_once('error_handler.php');

