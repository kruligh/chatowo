<?php
set_error_handler('errorHandler');
register_shutdown_function("shutdownHandler");
function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context)
{
    $error = " MSG: " . $error_message . " <br> FILE: " . $error_file . " <br> LINE: " . $error_line;
    switch ($error_level) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_PARSE:
            mylog($error, "fatal");
            break;
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
            mylog($error, "error");
            break;
        case E_WARNING:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_USER_WARNING:
            mylog($error, "warn");
            break;
        case E_NOTICE:
        case E_USER_NOTICE:
            mylog($error, "info");
            break;
        case E_STRICT:
            mylog($error, "debug");
            break;
        default:
            mylog($error, "warn");
    }
}

function shutdownHandler() //will be called when php script ends.
{
    $lasterror = error_get_last();
    switch ($lasterror['type'])
    {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_PARSE:
            $error = "msg:" . $lasterror['message'] . " <br> file:" . $lasterror['file'] . " <br> ln:" . $lasterror['line'];
            mylog($error, "fatal");
    }
}

function mylog($error, $errlvl)
{
    header("status",true,400);
    echo "ERROR LEVEL: ".$errlvl." ERROR: ".$error;
    //todo save to logs
    die();
}