<?php
require_once(dirname(__DIR__).'/database/SessionManager.php');

$session = null;
$headers = apache_request_headers();
$keys = array_keys($headers);
if(isset($_COOKIE["x-auth-token"])){

    $auth_token = $_COOKIE["x-auth-token"];
    $sessionDao = new SessionManager();
    $session = $sessionDao->findSessionByToken($auth_token);
    if(!$session){
        onError("INVALID_TOKEN");
    }
    $sessionDao->hitSession($session->getId());

}else{
    onError("NO_CREDENTIALS");
}

function onError($msg){
    removeCookies();
    header("status",true,403);
    echo $msg;
    die();
}


function removeCookies(){
    setcookie("x-auth-token", "", time() - 3600);
}