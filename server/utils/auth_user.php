<?php
include ('auth.php');


require_once(dirname(__DIR__).'/database/UserDbDao.php');

$userDao = new UserDbDao();

$user = $userDao->getUserInfo($session->getUserId());

if($user->getUserType() === 'ANONYM'){
    header("status",true,401);
    echo 'ANONYM USER';
    die();
}
