<?php
include ('post.php');
$input = file_get_contents("php://input");

if(!isset($input) || ($input == ""))
{
    header("status",true,400);
    echo "INVALID_ARGUMENTS";
    die();
}

$input = json_decode($input);

if(!isset($input) || ($input == ""))
{
    header("status",true,400);
    echo "INVALID_ARGUMENTS";
    die();
}