<?php
include('utils/headers.php');
include('utils/auth.php');
include('utils/post.php');
require_once('database/SubscriptionDao.php');

$inputData = file_get_contents("php://input");

if($inputData == false){
    header("status",true,400);
    echo "INVALID_ARGUMENTS";
    die();
}
$inputData = json_decode($inputData);

$subscriptionsDao = new SubscriptionDao();

$subscriptionsDao->unSubscribeRoom($inputData->subscriptionId);
