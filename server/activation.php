<?php
include('utils/headers.php');
require_once ('database/UserDbDao.php');

$token = $_REQUEST['token'];

if(!isset($token) || $token == ''){
    header("status",true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$userDao = new UserDbDao();

if(!($userDao->activateUser($token))){
    header("status",true,400);
    echo "ACTIVATION_FAIL";
    die();
}

echo "OK";
