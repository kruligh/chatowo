<?php
include('utils/headers.php');
include('utils/auth.php');
include('utils/post.php');
require_once('database/MessageDao.php');

include ('utils/post_input.php');

if(empty($input->roomId) || empty($input->content)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$messageDao = new MessageDao();

$messageDao->sendMessage($session->getUserId(), $input->roomId,$input->content);