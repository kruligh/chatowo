<?php
include('utils/headers.php');
include('utils/auth.php');
require_once('database/RoomsDao.php');


$roomsDao = new RoomsDao();

$rooms = $roomsDao->getAllRooms();

echo json_encode($rooms);

