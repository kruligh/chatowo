<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
require_once('DbConnection.php');

require_once(dirname(__DIR__).'/model/FriendDto.php');

class FriendsDao
{
    private $friendsTableName;
    private $userTableName;
    private $friendsWithNamesViewName;
    private $conn;

    public function __construct(){
        $this->friendsTableName = "FRIENDS";
        $this->friendsWithNamesViewName = "FRIENDS_WITH_NAMES";
        $this->conn = DbConnection::getConnection();
    }

    public function addFriend($fromUserId, $toUserId){
        $toUserId = mysqli_real_escape_string(DbConnection::getConnection(),$toUserId);

        $query = "INSERT INTO $this->friendsTableName (FROM_UID, TO_UID) VALUES($fromUserId, $toUserId)";

        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);

        }


    }

    public function deleteFriend($fromUserId, $toUserId){
        $toUserId = mysqli_real_escape_string(DbConnection::getConnection(),$toUserId);

        $query = "DELETE FROM $this->friendsTableName WHERE FROM_UID = $fromUserId AND TO_UID=$toUserId";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function findByUserId($userId){

        $resultArray = array();

        $query = "SELECT $this->friendsWithNamesViewName.*
from $this->friendsWithNamesViewName
where FROM_UID=$userId";

        $queryResult = $this->conn->query($query);

        if(!$queryResult){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }

        while($row = $queryResult->fetch_assoc()){

            $resultArray[] = new Friend($row['TO_UID'], $row['NICK']);
        }

        return $resultArray;
    }

    public function isFriendshipExists($userId, $toUserId)
    {
        $toUserId = mysqli_real_escape_string(DbConnection::getConnection(),$toUserId);
        $userId = mysqli_real_escape_string(DbConnection::getConnection(),$userId);

        $query = "SELECT * FROM $this->friendsTableName WHERE FROM_UID = $userId AND TO_UID = $toUserId";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);

        }
        if($result->num_rows > 0){
            return true;
        }else{
            return false;
        }
    }


}