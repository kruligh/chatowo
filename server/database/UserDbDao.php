<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
require_once('DbConnection.php');
require_once(dirname(__DIR__).'/model/User.php');
require_once(dirname(__DIR__).'/model/UserFullInfoDTO.php');
require_once(dirname(__DIR__).'/model/MyUserFullInfoDTO.php');
require_once(dirname(__DIR__).'/model/UserToEmailNotify.php');

class UserDbDao
{

    private $userTableName;
    private $conn;

    function __construct() {
        $this->userTableName = "USER";
        $this->conn = DbConnection::getConnection();

    }

    public function findActiveByNick($nick){
        $nick = mysqli_real_escape_string(DbConnection::getConnection(),$nick);
        $query = "SELECT id FROM $this->userTableName WHERE nick = '$nick' AND (active = 1 OR USER_TYPE != 'ANONYM')";

        $result = $this->conn->query($query);

        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }else if($result->num_rows > 1){
            DbConnection::databaseError("Multiple Rows in query: ".$query.__FILE__.':'.__LINE__);
        }
        $result = $result->fetch_assoc();
        return $result['id'];
    }

    public function findByEmail($email){
        $email = mysqli_real_escape_string(DbConnection::getConnection(),$email);

        $query = "SELECT id FROM $this->userTableName WHERE email = '$email' or nick ='$email'";

        $result = $this->conn->query($query);

        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }else if($result->num_rows > 1){
            DbConnection::databaseError("Multiple Rows in query: ".$query.__FILE__.':'.__LINE__);
        }
        $result = $result->fetch_assoc();
        return $result['id'];
    }

    public function insertAnonym($nick,$city,$birthday, $sex){
        $nick = mysqli_real_escape_string(DbConnection::getConnection(),$nick);
        $sex = mysqli_real_escape_string(DbConnection::getConnection(),$sex);
        $city = mysqli_real_escape_string(DbConnection::getConnection(),$city);
        $birthday = mysqli_real_escape_string(DbConnection::getConnection(),$birthday);


        $query = "INSERT INTO $this->userTableName (USER_TYPE, NICK, CITY, BIRTHDAY, SEX, ACTIVE) VALUES('ANONYM','$nick', '$city','$birthday','$sex', 1);";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
        return $result;
    }

    public function registerUser($nick, $email, $password, $salt,$city, $birthday, $sex, $activationToken){
        $nick = mysqli_real_escape_string(DbConnection::getConnection(),$nick);
        $email = mysqli_real_escape_string(DbConnection::getConnection(),$email);
        $password = mysqli_real_escape_string(DbConnection::getConnection(),$password);
        $password = $this->hashWithSalt($password, $salt);
        $city = mysqli_real_escape_string(DbConnection::getConnection(),$city);
        $birthday = mysqli_real_escape_string(DbConnection::getConnection(),$birthday);
        $sex = mysqli_real_escape_string(DbConnection::getConnection(),$sex);
        $status = "Cześć, jestem tu nowy!";

        $query = "INSERT INTO $this->userTableName (USER_TYPE, NICK, EMAIL, PASSWORD, SALT, SEX, ACTIVE, ACTIVATION_TOKEN, STATUS, CITY, BIRTHDAY) VALUES('USER', '$nick', '$email' , '$password', '$salt', $sex, 0, '$activationToken','$status','$city','$birthday');";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function findUserById($userId)
    {
        $userId = mysqli_real_escape_string(DbConnection::getConnection(),$userId);
        $query = "SELECT $this->userTableName.id, $this->userTableName.user_type, $this->userTableName.nick, $this->userTableName.active 
                      FROM $this->userTableName WHERE ID = $userId";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }

        $result = $result->fetch_assoc();

        return new User($result['id'],$result['user_type'], $result['nick'], $result['active']);

    }

    public function activateUser($token){
        $token = mysqli_real_escape_string(DbConnection::getConnection(),$token);
        $query = "UPDATE $this->userTableName SET ACTIVE = 1 WHERE ACTIVATION_TOKEN = '$token'";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }else if($this->conn->affected_rows == 0){
            return false;
        }else{
            return true;
        }
    }

    public function checkCredentials($login, $password){
        $login = mysqli_real_escape_string(DbConnection::getConnection(),$login);
        $password = mysqli_real_escape_string(DbConnection::getConnection(),$password);

        $query = "SELECT $this->userTableName.ID, $this->userTableName.PASSWORD, $this->userTableName.SALT FROM $this->userTableName WHERE EMAIL = '$login' OR NICK = '$login';";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
        $result = $result->fetch_assoc();
        $password = $this->hashWithSalt($password, $result['SALT']);
        if($result['PASSWORD']===$password){
            return $result['ID'];
        }
        return null;
    }

    public function getMyUserInfo($userId)
    {
        $userId = mysqli_real_escape_string(DbConnection::getConnection(),$userId);

        $query = "SELECT $this->userTableName.*
                      FROM $this->userTableName WHERE ID = $userId";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }

        $result = $result->fetch_assoc();

        if($result['USER_TYPE']==='ANONYM'){
            $result['STATUS'] = 'Anonim';
        }

        return new MyUserFullInfoDTO($result['ID'],$result['USER_TYPE'], $result['NICK'], $result['SEX'], $result['EMAIL'], $result['STATUS'], $result['CITY'], $result['BIRTHDAY'], $result['ACTIVE']);

    }

    public function getUserInfo($userId)
    {
        $userId = mysqli_real_escape_string(DbConnection::getConnection(),$userId);

        $query = "SELECT $this->userTableName.*
                      FROM $this->userTableName WHERE ID = $userId";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }

        $result = $result->fetch_assoc();

        if($result['USER_TYPE']==='ANONYM'){
            $result['STATUS'] = 'Anonim';
        }

        return new UserFullInfoDTO($result['ID'],$result['USER_TYPE'], $result['NICK'], $result['SEX'], $result['STATUS'], $result['CITY'], $result['BIRTHDAY']);

    }

    public function checkIfNickExists($nick)
    {
        return !empty($this->findByNick($nick));
    }

    public function checkIfEmailExists($email)
    {
        return !empty($this->findByEmail($email));
    }

    public function setUserStatus($userId, $status)
    {
        $status = mysqli_real_escape_string(DbConnection::getConnection(),$status);

        $query = "UPDATE $this->userTableName SET STATUS = '$status' WHERE ID = $userId";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }else if($this->conn->affected_rows == 0){
            return false;
        }else{
            return true;
        }

    }

    public function updateUser($newUser){
        $nick = mysqli_real_escape_string(DbConnection::getConnection(),$newUser->nick);
        $city = mysqli_real_escape_string(DbConnection::getConnection(),$newUser->city);
        $birthday =mysqli_real_escape_string(DbConnection::getConnection(), $newUser->birthday);
        $status = mysqli_real_escape_string(DbConnection::getConnection(), $newUser->status);

        $query = "UPDATE $this->userTableName SET NICK = '$nick', CITY = '$city', BIRTHDAY = '$birthday', STATUS='$status' 
WHERE $this->userTableName.ID = $newUser->id";

        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }else if($this->conn->affected_rows == 0){
            return false;
        }else{
            return true;
        }

    }

    private function hashWithSalt($password, $salt){
        return sha1($salt.$password);

    }

    public function getUserByEmail($login)
    {
        $login = mysqli_real_escape_string(DbConnection::getConnection(),$login);

        $query = "SELECT $this->userTableName.*
                      FROM $this->userTableName WHERE EMAIL = '$login' AND ACTIVE = 1";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }

        $result = $result->fetch_assoc();

        if(!empty($result)){
            return new UserToEmailNotify($result['ID'], $result['NICK'], $result['EMAIL']);
        }else{
            return null;
        }



    }

    public function changePassword($userId, $newPassword)
    {
        $password = mysqli_real_escape_string(DbConnection::getConnection(),$newPassword);

        $query = "UPDATE $this->userTableName SET PASSWORD = '$password' WHERE ID = $userId";
        $result = $this->conn->query($query);
        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }else if($this->conn->affected_rows == 0){
            return false;
        }else{
            return true;
        }
    }

}
