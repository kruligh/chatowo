<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
require_once('DbConnection.php');
require_once(dirname(__DIR__).'/model/Subscription.php');

class SubscriptionDao
{
    private $subscriptionTableName;
    private $roomTableName;
    private $userTableName;
    private $conn;

    public function __construct()
    {
        $this->subscriptionTableName = "SUBSCRIPTION";
        $this->roomTableName = "ROOM";
        $this->userTableName = "USER";
        $this->conn = DbConnection::getConnection();
    }

    public function subscribeRoom($userId, $roomId){

        $userId = mysqli_real_escape_string(DbConnection::getConnection(),$userId);
        $roomId= mysqli_real_escape_string(DbConnection::getConnection(),$roomId);

        $query = "INSERT INTO $this->subscriptionTableName (USER_ID, ROOM_ID, PRIV_USER_ID) VALUES($userId, $roomId, NULL)";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function unSubscribeRoom($subscribeId)
    {
        $subscribeId= mysqli_real_escape_string(DbConnection::getConnection(),$subscribeId);

        $query = "DELETE FROM $this->subscriptionTableName WHERE ID= $subscribeId";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function findByUserId($userId){
        $userId= mysqli_real_escape_string(DbConnection::getConnection(),$userId);

        $resultArray = array();

        $query = "SELECT $this->subscriptionTableName.ID, $this->subscriptionTableName.ROOM_ID , $this->roomTableName.NAME , $this->subscriptionTableName.ADD_DATE 
FROM $this->subscriptionTableName, $this->roomTableName 
WHERE $this->subscriptionTableName.USER_ID = $userId 
AND $this->subscriptionTableName.ROOM_ID = $this->roomTableName.ID";

        $queryPriv = "SELECT $this->subscriptionTableName.ID, $this->subscriptionTableName.PRIV_USER_ID, $this->userTableName.NICK, $this->subscriptionTableName.ADD_DATE 
FROM $this->subscriptionTableName, $this->userTableName 
WHERE $this->subscriptionTableName.USER_ID = $userId 
AND $this->subscriptionTableName.PRIV_USER_ID = $this->userTableName.ID";

        $queryResult = $this->conn->query($query);
        $queryPrivResult = $this->conn->query($queryPriv);

        if(!$queryResult){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }

        if(!$queryPrivResult){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }

        while($row = $queryResult->fetch_assoc()){
            $resultArray[] = new Subscription($row['ID'],$row['ROOM_ID'], NULL, $row['NAME'], $row['ADD_DATE']);
        }


        while($row = $queryPrivResult->fetch_assoc()){
            $resultArray[] = new Subscription($row['ID'], NULL, $row['PRIV_USER_ID'], $row['NICK'], $row['ADD_DATE']);
        }

        return $resultArray;
    }

    public function findById($subId){
        $subId= mysqli_real_escape_string(DbConnection::getConnection(),$subId);

        $query = "SELECT * FROM $this->subscriptionTableName WHERE ID = $subId";

        $result = $this->conn->query($query);

        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }else if($result->num_rows > 1){
            DbConnection::databaseError("Multiple Rows in query: ".$query);
        }

        $row = $result->fetch_assoc();

        return new Subscription($row['ID'],$row['ROOM_ID'], null, $row['ADD_DATE']);

    }

    public function createPriv($userId,$secondUserId){
        $secondUserId = mysqli_real_escape_string(DbConnection::getConnection(),$secondUserId);

        if(!$this->privExists($userId, $secondUserId)) {
            $this->subscribePriv($userId, $secondUserId);
        }

        if(!$this->privExists($secondUserId, $userId)){
            $this->subscribePriv($secondUserId, $userId);
        }
    }

    private function subscribePriv($userId, $secondUserId){
        $query = "INSERT INTO $this->subscriptionTableName (USER_ID, ROOM_ID, PRIV_USER_ID) VALUES($userId, NULL, $secondUserId)";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function privExists($userId, $secondUserId){
        $query = "SELECT * FROM $this->subscriptionTableName WHERE USER_ID = $userId AND PRIV_USER_ID = $secondUserId";

        $result = $this->conn->query($query);

        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }

        if($result->num_rows > 0){
            return true;
        }else{
            return false;
        }
    }


}