<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
require_once('DbConnection.php');
require_once(dirname(__DIR__).'/model/Subscription.php');
require_once(dirname(__DIR__).'/model/ChatMessage.php');
require_once(dirname(__DIR__).'/model/User.php');

class MessageDao
{
    private $messageTableName;
    private $conn;
    private $userTableName;

    public function __construct(){
        $this->messageTableName = "MESSAGE";
        $this->userTableName = "USER";
        $this->privMessageTableName ="PRIV_MESSAGE";
        $this->conn = DbConnection::getConnection();
    }

    public function sendMessage($userId, $roomId, $content)
    {
        $userId = mysqli_real_escape_string(DbConnection::getConnection(),$userId);
        $roomId = mysqli_real_escape_string(DbConnection::getConnection(),$roomId);
        $content = mysqli_real_escape_string(DbConnection::getConnection(),$content);

        $query = "INSERT INTO $this->messageTableName (FROM_UID, ROOM_ID, CONTENT) VALUES($userId, $roomId, '$content')";

        $result = $this->conn->query($query);

        if(!$result){
            DbConnection::databaseError($query);
        }
    }

    public function sendPrivMessage($userId, $toUserId, $content)
    {
        $userId = mysqli_real_escape_string(DbConnection::getConnection(),$userId);
        $toUserId = mysqli_real_escape_string(DbConnection::getConnection(),$toUserId);
        $content = mysqli_real_escape_string(DbConnection::getConnection(),$content);

        $query = "INSERT INTO $this->privMessageTableName (FROM_UID, TO_UID, CONTENT) VALUES($userId, $toUserId, '$content')";

        $result = $this->conn->query($query);

        if(!$result){
            DbConnection::databaseError($query);
        }
    }

    public function getMessages($idRoom, $addDate)
    {
        $idRoom = mysqli_real_escape_string(DbConnection::getConnection(),$idRoom);
        $addDate = mysqli_real_escape_string(DbConnection::getConnection(),$addDate);

        $resultArray = array();
        $query = "SELECT $this->messageTableName.*, $this->userTableName.id as ID_USER, $this->userTableName.user_type as USER_TYPE, $this->userTableName.nick as NICK FROM $this->messageTableName, $this->userTableName WHERE $this->messageTableName.from_uid = $this->userTableName.ID AND ROOM_ID = $idRoom AND SEND_DATE>'$addDate'";

        $queryResult = $this->conn->query($query);

        if(!$queryResult){
            DbConnection::databaseError($query);
        }
        while($row = $queryResult->fetch_assoc()) {
            $user = new User($row['ID_USER'],$row['USER_TYPE'], $row['NICK'], 1);
            $resultArray[] = new ChatMessage($row['ID'],$row['CONTENT'],$user, $row['SEND_DATE']);
        }
        return $resultArray;
    }


    public function getPrivMessages($userId, $privUserId, $addDate){

        $resultArray = array();
        $query = "SELECT $this->privMessageTableName.*, $this->userTableName.id as ID_USER, $this->userTableName.user_type as USER_TYPE, $this->userTableName.nick as NICK 
FROM $this->privMessageTableName, $this->userTableName 
WHERE $this->privMessageTableName.from_uid = $this->userTableName.ID 
AND ((TO_UID = $userId AND FROM_UID = $privUserId) OR (TO_UID = $privUserId AND FROM_UID = $userId)) 
AND SEND_DATE>'$addDate'";

        $queryResult = $this->conn->query($query);

        if(!$queryResult){
            DbConnection::databaseError($query);
        }
        while($row = $queryResult->fetch_assoc()) {
            $user = new User($row['ID_USER'],$row['USER_TYPE'], $row['NICK'], 1);
            $resultArray[] = new ChatMessage($row['ID'],$row['CONTENT'],$user, $row['SEND_DATE']);
        }
        return $resultArray;


    }



}