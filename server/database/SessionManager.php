<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
require_once('DbConnection.php');
require_once(dirname(__DIR__).'/model/Session.php');

class SessionManager{
    private $sessionTableName;
    private $conn;

    function __construct() {
        $this->sessionTableName = "SESSION";

        $this->conn = DbConnection::getConnection();
    }

    public function createSession($userId){
        $userId = mysqli_real_escape_string(DbConnection::getConnection(),$userId);
        $token = null;
        do{
            $token = md5(rand(0,9999));

            $query = "SELECT * FROM $this->sessionTableName WHERE token = '$token' AND active = 1;";

            $result = $this->conn->query($query);

            if($result == false){
                DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
            }
        }while($result->num_rows > 0);

        $query = "INSERT INTO $this->sessionTableName (USER_ID, TOKEN, ACTIVE) VALUES($userId,'$token',1)";

        $result = $this->conn->query($query);

        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
        return $token;
    }

    public function findSessionByToken($auth_token){
        $auth_token = mysqli_real_escape_string(DbConnection::getConnection(),$auth_token);

        $query = "SELECT * FROM $this->sessionTableName where ACTIVE = 1 AND token = '$auth_token'";
        $session = $this->conn->query($query);

        if($session->num_rows == 0){
            return false;
        }else if($session->num_rows >1){
            DbConnection::databaseError("Multiple Rows in query: ".$query);
        }
        $session = $session->fetch_assoc();

        return new Session($session['ID'], $session['USER_ID'], $session['TOKEN']);
    }

    /*
     * Database trigger should delete subscriptions and archive anonym users
     */
    public function deactiveSession($sessionId){

        $sessionId = mysqli_real_escape_string(DbConnection::getConnection(),$sessionId);
       // TODO Database trigger should delete subscriptions and archive anonym users
        //$query = "DELETE FROM $this->sessionTableName WHERE id = $sessionId";

        $query = "UPDATE $this->sessionTableName SET ACTIVE = 0 WHERE id = $sessionId";

        $result = $this->conn->query($query);

        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function hitSession($getId)
    {
        $query = "UPDATE $this->sessionTableName SET LAST_HIT = CURRENT_TIMESTAMP where ID = '$getId'";

        $result = $this->conn->query($query);
    }
}
?>