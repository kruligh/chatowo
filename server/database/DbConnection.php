<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
class DbConnection
{
    private static $conn;

    public static function getConnection(){
        if(!isset(self::$conn)){

            self::$conn = new mysqli(DbConfig::SERVER_URL, DbConfig::USERNAME, DbConfig::PASSWORD, DbConfig::DB_NAME);
            self::$conn->set_charset('utf-8');
            if (self::$conn->connect_error){
                header("status",true,400);
                echo 'DATABASE_ERROR: '.self::$conn->connect_error;
                die();
            }
        }
        return self::$conn;
    }

    public static function databaseError($msg =""){
        header("status",true,400);
        //FIXME USUNAC TO ECHO XDD ALE DLA BADANIA JEZYKA PHP POMOCNE
        echo 'DATABASE_ERROR '.$msg;
        self::$conn->rollback();
        die();
    }
}
?>