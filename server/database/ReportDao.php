<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
require_once('DbConnection.php');
require_once(dirname(__DIR__).'/model/Report.php');

class ReportDao{
    private $reportTableName;
    private $userTableName;
    private $conn;

    public function __construct(){
        $this->reportTableName = "REPORT";
        $this->userTableName = "USER";
        $this->reportViewName = "REPORT_MSG_CONTENT_VIEW";
        $this->conn = DbConnection::getConnection();
    }

    public function reportMessage($messageId, $privMessageId, $reason){
        $messageId = mysqli_real_escape_string(DbConnection::getConnection(),$messageId);
        $privMessageId = mysqli_real_escape_string(DbConnection::getConnection(),$privMessageId);
        $reason = mysqli_real_escape_string(DbConnection::getConnection(),$reason);

        if(!empty($privMessageId)){
            $query = "INSERT INTO $this->reportTableName (MESSAGE_PRIV_ID, REASON) VALUES($privMessageId, '$reason')";

        }else{
            $query = "INSERT INTO $this->reportTableName ( MESSAGE_ID, REASON) VALUES($messageId, '$reason')";

        }
         $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function getNewReports(){

        $resultArray = array();
        $query = "SELECT * FROM $this->reportViewName where CONSUMED = 0";

        $queryResult = $this->conn->query($query);

        if(!$queryResult){
            DbConnection::databaseError($query);
        }
        while($row = $queryResult->fetch_assoc()) {
            $user = new User($row['USER_ID'],$row['USER_TYPE'], $row['NICK'], 1);
            $resultArray[] = new Report($row['REPORT_ID'],$user, $row['CONTENT'], $row['REASON']);
        }
        return $resultArray;
    }

    public function consumeReport($reportId){
        $reportId = mysqli_real_escape_string(DbConnection::getConnection(),$reportId);

        $query = "UPDATE $this->reportTableName SET CONSUMED = 1 WHERE ID = '$reportId'";

        $queryResult = $this->conn->query($query);

        if(!$queryResult){
            DbConnection::databaseError($query);
        }
    }

}