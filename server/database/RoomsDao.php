<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
require_once('DbConnection.php');
require_once(dirname(__DIR__).'/model/Room.php');
class RoomsDao
{
    private $roomTableName;
    private $subscriptionTableName;
    private $conn;

    public function __construct()
    {
        $this->roomTableName = "ROOM";
        $this->subscriptionTableName = "SUBSCRIPTION";
        $this->conn = DbConnection::getConnection();
    }

    public function getAllRooms()
    {
        $resultArray = array();

        //$query = "SELECT * FROM $this->roomTableName";
        $query = "SELECT $this->roomTableName.*, COUNT($this->subscriptionTableName.ID) AS USERS_COUNT FROM $this->roomTableName ".
            "LEFT JOIN $this->subscriptionTableName ON $this->roomTableName.ID = $this->subscriptionTableName.ROOM_ID 
GROUP BY $this->roomTableName.ID ";

        $queryResult = $this->conn->query($query);

        if(!$queryResult){
            DbConnection::databaseError($query);
        }

        while($row = $queryResult->fetch_assoc()) {
            $resultArray[] = new Room($row['ID'], $row['NAME'], $row['USERS_COUNT']);
        }
        return $resultArray;
    }

    public function editRoomName($roomId, $newRoomName){
        $roomId = mysqli_real_escape_string(DbConnection::getConnection(), $roomId);
        $newRoomName = mysqli_real_escape_string(DbConnection::getConnection(), $newRoomName);

        $query = "UPDATE $this->roomTableName SET NAME = '$newRoomName' WHERE ID = $roomId";

        $queryResult = $this->conn->query($query);

        if(!$queryResult) {
            DbConnection::databaseError($query);
        }else if($this->conn->affected_rows == 0){
            DbConnection::databaseError("Room with id: ".$roomId." does not exists");
        }
    }

    public function deleteRoom($roomId){
        $roomId = mysqli_real_escape_string(DbConnection::getConnection(), $roomId);

        $query = "DELETE FROM $this->roomTableName WHERE ID = $roomId";

        $queryResult = $this->conn->query($query);

        if(!$queryResult) {
            DbConnection::databaseError($query);
        }else if($this->conn->affected_rows == 0){
            DbConnection::databaseError("Room with id: ".$roomId." does not exists");
        }
    }

    public function addRoom($roomName){
        $roomName = mysqli_real_escape_string(DbConnection::getConnection(), $roomName);

        $query = "INSERT INTO ROOM(NAME) VALUES('$roomName')";

        $queryResult = $this->conn->query($query);

        if(!$queryResult) {
            DbConnection::databaseError($query);
        }else if($this->conn->affected_rows == 0){
            DbConnection::databaseError("something wrong");
        }
    }


}