<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
require_once('DbConnection.php');

class LogsDao{
    private $logsTableName;
    private $conn;

    public function __construct(){
        $this->logsTableName = "LOGS";
        $this->conn = DbConnection::getConnection();
    }

    public function falseLogin($ip, $login){
        $query = "INSERT INTO $this->logsTableName (IP_ADDRESS, LOGIN, LAST_FALSE) VALUES('$ip','$login', 1)";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }



    public function attemptLoginCount($ip,$login){
        $login = mysqli_real_escape_string(DbConnection::getConnection(),$login);
        $query = "SELECT COUNT(*) as logCount FROM $this->logsTableName WHERE (LOGIN = '$login' OR IP_ADDRESS = '$ip') AND LAST_FALSE= 1";

        $queryResult = $this->conn->query($query);

        if(!$queryResult){
            DbConnection::databaseError($query);
        }
        $queryResult = $queryResult->fetch_assoc();
        return $queryResult['logCount'];
    }

    public function unactiveFalseLogs($ip, $login){
        $login = mysqli_real_escape_string(DbConnection::getConnection(),$login);
        $query = "UPDATE $this->logsTableName SET LAST_FALSE = 0 WHERE LOGIN = '$login' or IP_ADDRESS = '$ip'";

        $result = $this->conn->query($query);

        if($result == false){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function getLogins($ip)
    {
        $query = "SELECT * FROM $this->logsTableName WHERE IP_ADDRESS = '$ip' AND LAST_FALSE= 1";

        $queryResult = $this->conn->query($query);

        if(!$queryResult){
            DbConnection::databaseError($query);
        }
        $resultArray = array();
        while($row = $queryResult->fetch_assoc()) {
            $resultArray[] = $queryResult['LOGIN'];
        }
        return $resultArray;
    }
}