<?php
require_once(dirname(__DIR__).'/config/DbConfig.php');
require_once('DbConnection.php');

class BanDao{
    private $banTableName;
    private $conn;

    public function __construct(){
        $this->banTableName = "BAN";
        $this->conn = DbConnection::getConnection();
    }

    public function loginBanByIp($ip){
        $reason = "Niepoprawny ciąg logowań";
        $query = "INSERT INTO $this->banTableName (IP_ADDRESS, REASON, TO_DATE) VALUES('$ip', '$reason',CURRENT_TIMESTAMP + 600)";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function loginBan($ip, $userId){
        $reason = "Niepoprawny ciąg logowań";
        $query = "INSERT INTO $this->banTableName (IP_ADDRESS,USER_ID, REASON, TO_DATE) VALUES('$ip',$userId, '$reason',CURRENT_TIMESTAMP + 600)";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }

    public function hasPossibilityLoginByIp($ip){
        $query = "SELECT * FROM $this->banTableName WHERE IP_ADDRESS = '$ip'AND (TO_DATE IS NULL OR TO_DATE> CURRENT_TIMESTAMP )";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
        $result = $result->fetch_assoc();
        if(empty($result)){
            return true;
        }else{
            return $result['TO_DATE'];
        };
    }

    public function hasPossibilityLogin($ip, $userId){
        $query = "SELECT * FROM $this->banTableName WHERE (IP_ADDRESS = '$ip' OR USER_ID = '$userId') AND (TO_DATE IS NULL OR TO_DATE > CURRENT_TIMESTAMP )";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
        $result = $result->fetch_assoc();
        if(empty($result)){
            return true;
        }else{
            return $result['TO_DATE'];
        };
    }

    public function banUser($userId,$ip, $reason, $end, $adminId){
        $userId = mysqli_real_escape_string(DbConnection::getConnection(),$userId);
        $reason = mysqli_real_escape_string(DbConnection::getConnection(), $reason);
        $end = mysqli_real_escape_string(DbConnection::getConnection(),$end);

        $query = "INSERT INTO $this->banTableName (ADMIN_ID, USER_ID, IP_ADDRESS, REASON, TO_DATE) VALUES($adminId,$userId,'$ip','$reason', '$end')";
        $result = $this->conn->query($query);
        if(!$result){
            DbConnection::databaseError($query.__CLASS__.':'.__LINE__);
        }
    }
}