<?php
include('utils/headers.php');
include ('utils/post_input.php');
require_once('database/DbConnection.php');
require_once('database/SessionManager.php');
require_once('database/UserDbDao.php');
require_once('database/BanDao.php');
require_once ('class/Validator.php');

if( empty($input->nick) || empty($input->city) || empty($input->birthday)|| strlen($input->sex) ===0){
    header("status",true,400);
    echo "INVALID_ARGUMENTS";
    die();
}

Validator::validateNick($input->nick);

$conn = DbConnection::getConnection();

$user_dao = new UserDbDao();

$result = $user_dao->findActiveByNick($input->nick);
if(!empty($result)) {
    header("status",true,400);
    echo "uzytkownik o podanym loginie istnieje";
    die();
}

$ip = getenv('REMOTE_ADDR');
$banDao = new BanDao();
$possibilityLogin = $banDao->hasPossibilityLoginByIp($ip);
if($possibilityLogin!==true) {
    header("status",true,403);
    echo "Jesteś zbanowany, możliwe logowanie: ".$possibilityLogin;
    die();
}



$transactionStarted = $conn->begin_transaction();
    if(!($user_dao->insertAnonym($input->nick,$input->city, $input->birthday, $input->sex))){
        DbConnection::databaseError("SOMETHING_WRONG");
    };
    $result = $user_dao->findActiveByNick($input->nick);
    if(empty($result)){
        DbConnection::databaseError("User doesn't exist");
    }

    $session_manager = new SessionManager();
    $token = $session_manager->createSession($result);
    $sessionId = $session_manager->findSessionByToken($token)->getId();
    setcookie("x-auth-token",$token);
$conn->commit();

echo $token;

?>