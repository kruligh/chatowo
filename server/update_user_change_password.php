<?php
include ('utils/headers.php');
include ('utils/auth_user.php');
include ('utils/post_input.php');

require_once ('database/UserDbDao.php');

$userDao = new UserDbDao();

if( empty($input->oldPassword) || empty($input->newPassword)){
    header("status",true,400);
    echo "INVALID_ARGUMENTS";
    die();
}


if(!empty($userDao->checkCredentials($userDao->findUserById($session->getUserId())->getNick(),$input->oldPassword))){
    if(!$userDao->changePassword($session->getUserId(),$input->newPassword)){
        header("status",true,400);
        echo "DB_ERROR";
        die();
    }else{
        echo "OK";
    }
}else{
    header("status",true,403);
    echo "INVALID_CREDENTIALS";
    die();
}