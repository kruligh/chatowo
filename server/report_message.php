<?php
include('utils/headers.php');
include('utils/auth.php');
include('utils/post.php');
require_once('database/ReportDao.php');

include ('utils/post_input.php');

if((empty($input->messageId) && empty($input->privMessageId) )|| empty($input->reason)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$reportDao = new ReportDao();

$reportDao->reportMessage($input->messageId, $input->privMessageId, $input->reason);