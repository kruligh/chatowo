<?php
include('utils/headers.php');
include ('utils/auth_user.php');
require_once('database/DbConnection.php');
require_once('database/FriendsDao.php');
require_once('model/Session.php');

$friendsDao = new FriendsDao();

$result = $friendsDao->findByUserId($session->getUserId());

$result = json_encode($result);

if(empty($result)){
    header('status',true,400);
    echo 'EMPTY_RESPONSE'.$result;
    die();
}

echo $result;