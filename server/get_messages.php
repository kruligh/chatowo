<?php
include('utils/headers.php');
include('utils/auth.php');
require_once('database/MessageDao.php');
require_once('database/SubscriptionDao.php');
require_once ('model/RoomsMessagesDTO.php');

$messageDao = new MessageDao();
$subscriptionDao = new SubscriptionDao();

$subscriptions = $subscriptionDao->findByUserId($session->getUserId());

$messages = array();

foreach($subscriptions as $subscription){
    if($subscription->isPriv()){
        $result = $messageDao->getPrivMessages($session->getUserId(), $subscription->getPrivUserId(), $subscription->getAddDate());
        $messages[] = new RoomsMessagesDTO(null,$subscription->getPrivUserId(),$result);
    }else{
        $result = $messageDao->getMessages($subscription->getRoomId(), $subscription->getAddDate());
        $messages[] = new RoomsMessagesDTO($subscription->getRoomId(),null,$result);
    }

}

/*refreshVToken($session->getId());*/
echo json_encode($messages);