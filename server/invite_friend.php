<?php
include ('utils/headers.php');
include ('database/DbConnection.php');
include ('utils/auth.php');
require_once ('database/friendsDao.php');
require_once ('database/SubscriptionDao.php');
require_once ('model/friendsDto.php');

include('utils/post_input.php');

if(empty($input->userId)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$conn = DbConnection::getConnection();
$friendsDao = new FriendsDao();
$subscriptionDao = new SubscriptionDao();

$conn->begin_transaction();

if(!($friendsDao->createFriends($input->userId, $session->getUserId(),time()))){
    header('status',true,400);
    echo 'INVALID_SOMETHING';
    die();
}

if(!($subscriptionDao->createPriv($input->userId, $session->getUserId(), time()))){
    header('status',true,400);
    echo 'INVALID_SOMETHING';
    die();
}

$conn->commit();


