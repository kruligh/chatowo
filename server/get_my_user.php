<?php
include('utils/headers.php');
include('utils/auth.php');

require_once('database/UserDbDao.php');

$userDao = new UserDbDao();

$result = $userDao->getMyUserInfo($session->getUserId());

$result = json_encode($result);

if(empty($result)){
    header('status',true,400);
    echo 'EMPTY_RESPONSE'.$result;
    die();
}

echo $result;