<?php
include ('utils/headers.php');
include ('utils/auth_admin.php');
include ('utils/post_input.php');

require_once ('database/BanDao.php');
require_once ('database/ReportDao.php');

if(empty($input->userId) || empty($input->reason) || empty($input->end)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$ip = getenv('REMOTE_ADDR');
$banDao = new BanDao();

if(!empty($input->reportId)){
    $reportDao = new ReportDao();
    $reportDao->consumeReport($input->reportId);
}
$banDao->banUser($input->userId, $ip, $input->reason, $input->end, $session->getUserId());

