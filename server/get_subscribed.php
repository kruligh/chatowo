<?php
include('utils/headers.php');
include('utils/auth.php');
require_once('database/DbConnection.php');
require_once('database/SubscriptionDao.php');
require_once('model/Session.php');

$subscriptionDao = new SubscriptionDao();

$result = $subscriptionDao->findByUserId($session->getUserId());

echo json_encode($result);