<?php
include ('utils/headers.php');
include ('utils/auth_user.php');
include ('utils/post_input.php');

require_once ('database/UserDbDao.php');
require_once ('class/Validator.php');

$userDao = new UserDbDao();


Validator::validateNick($input->nick);
Validator::validateCity($input->city);
Validator::validateStatus($input->status);


$userDao->updateUser($input);
