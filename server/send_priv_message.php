<?php
include ('utils/headers.php');
include ('utils/auth.php');
include ('utils/post_input.php');

require_once ('database/MessageDao.php');
require_once ('database/SubscriptionDao.php');

if(empty($input->toUserId) || empty($input->content)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$messageDao = new MessageDao();
$subscriptionsDao = new SubscriptionDao();

if(!($subscriptionsDao->privExists($input->toUserId, $session->getUserId()))){
    $messageDao->sendPrivMessage($session->getUserId(), $input->toUserId, "Uzytkownik zamknął czat");
}else{
    $messageDao->sendPrivMessage($session->getUserId(), $input->toUserId, $input->content);
}



