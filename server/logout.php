<?php
include('utils/headers.php');
include('utils/auth.php');
include('utils/post.php');

require_once('database/SessionManager.php');
require_once('model/Session.php');
require_once ('database/UserDbDao.php');

$auth_token = $session->getToken();

$sessionDao = new SessionManager();
$userDao = new UserDbDao();

$sessionDao->deactiveSession($session->getId());

removeCookies();
?>