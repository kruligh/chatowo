<?php
include ('utils/headers.php');
include ('utils/auth_user.php');
include ('utils/post_input.php');

require_once ('database/FriendsDao.php');


if(empty($input->userId)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$friendsDao = new FriendsDao();

$friendsDao->deleteFriend($session->getUserId(), $input->userId);