<?php
include('utils/headers.php');
include('utils/auth.php');
include('utils/post_input.php');
require_once('database/SubscriptionDao.php');
require_once('database/MessageDao.php');


$subscriptionsDao = new SubscriptionDao();
$messageDao = new MessageDao();


$messageDao->sendMessage($session->getUserId(), $input->id, "Użytkownik dołączył do czatu");
$subscriptionsDao->subscribeRoom($session->getUserId(), $input->id);
