<?php
include('utils/headers.php');
include ('utils/post.php');
require_once ('database/DbConnection.php');
require_once ('database/UserDbDao.php');
require_once ('database/SessionManager.php');
require_once ('database/LogsDao.php');
require_once ('database/BanDao.php');
require_once ('model/MailBody.php');
require_once ('class/MailSender.php');

include('utils/post_input.php');

if(empty($input->login) || empty($input->password)) {
    header("status",true,400);
    echo "INVALID_ARGUMENTS";
    die();
}

$userDao = new UserDbDao();
$sessionDao = new SessionManager();

$conn = DbConnection::getConnection();

$conn->begin_transaction();
    $logs_dao = new LogsDao();
    $ban_dao = new BanDao();
    $ip = getenv('REMOTE_ADDR');

    $userId = $userDao->findByEmail($input->login);

    if(!empty($userId)){
        $possibilityLogin = $ban_dao->hasPossibilityLogin($ip,$userId);
    }else{
        $possibilityLogin = $ban_dao->hasPossibilityLoginByIp($ip);
    }


    if($possibilityLogin!==true) {
        header("status",true,403);
        echo "Jesteś zbanowany, możliwe logowanie: ".$possibilityLogin;
        die();
    }

    $userId = $userDao->checkCredentials($input->login, $input->password);
    if(empty($userId)){
        $logs_dao->falseLogin($ip, $input->login);
        $attemptLoginCount = $logs_dao->attemptLoginCount($ip, $input->login);
        if($attemptLoginCount>4){
            $user = $userDao->getUserByEmail($input->login);
            if(!empty($user)){
                $ban_dao->loginBan($ip, $user->getId());
                $mailer = new MailSender();
                $mailBody = $mailer->createFailLoginBanMail($user->getEmail(),$user->getNick());
                $mailer->sendMail($mailBody);
            }else{
                $ban_dao->loginBanByIp($ip);
            }
            $logs_dao->unactiveFalseLogs($ip, $input->login);
        }
        $conn->commit();
        header("status",true,403);
        echo "Podałeś niepoprawne dane ".$attemptLoginCount." razy!";
        die();
    }

    $logs_dao->unactiveFalseLogs($ip,$input->login);


    $user = $userDao->findUserById($userId);

    $ban_dao->hasPossibilityLogin("",$userId);

    if(!$user->isActive()){
        header("status",true,403);
        echo "Aktywuj email";
        die();
    }

    $session_manager = new SessionManager();
    $token = $sessionDao->createSession($userId);
    $sessionId = $session_manager->findSessionByToken($token)->getId();
    setcookie("x-auth-token",$token);
$conn->commit();

echo $token;
