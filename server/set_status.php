<?php
include ('utils/headers.php');
include ('utils/auth_user.php');
include('utils/post_input.php');

require_once ('database/UserDbDao.php');
require_once ('class/Validator.php');

$status = $input->newStatus;

Validator::validateStatus($status);
//todo tylko nie anonimy moga dodac status
if(empty($status)){
    header('status', true, 400);
    echo 'INVALID_DATA';
    die();
}

$userDao = new UserDbDao();

$result = $userDao->setUserStatus($session->getUserId(), $status);

if(!$result){
    header('status', true, 400);
    echo 'INVALID_SOMETHING';
    die();
}else{
    echo 'OK';
}