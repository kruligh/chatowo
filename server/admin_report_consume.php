<?php
include('utils/headers.php');
include('utils/auth_admin.php');
include ('utils/post_input.php');
require_once('database/ReportDao.php');

if(empty($input->reportId)){
    header('status',true,400);
    echo 'INVALID_ARGUMENTS';
    die();
}

$reportDao = new ReportDao();

$reportDao->consumeReport($input->reportId);