<?php

class RoomsMessagesDTO implements JsonSerializable
{

    private $roomId;
    private $privUserId;
    private $messages;
    public function __construct($roomId,$userId, $messages)
    {
        $this->roomId = $roomId;
        $this->privUserId = $userId;
        $this->messages = $messages;
    }

    function jsonSerialize()
    {
        return [
            'roomId'=>$this->roomId,
            'privUserId'=>$this->privUserId,
            'messages'=> $this->messages
        ];
    }


}