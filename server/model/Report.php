<?php

class Report implements JsonSerializable
{
    private $id;
    private $user;
    private $messageContent;
    private $reason;

    public function __construct($id, $user, $messageContent, $reason)
    {
        $this->id = $id;
        $this->user = $user;
        $this->messageContent = $messageContent;
        $this->reason = $reason;
    }


    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'user' => $this->user,
            'messageContent' => $this->messageContent,
            'reason' => $this->reason
        ];
    }
}