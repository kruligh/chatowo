<?php

class Room implements JsonSerializable
{
    private $id;
    private $name;
    private $usersCount;

    function __construct($id, $name, $usersCount)
    {
        $this->id = $id;
        $this->name = $name;
        $this->usersCount = $usersCount;
    }

    function getId(){
        return $this->id;
    }

    function getName()
    {
        return $this->name;
    }

    public function getUsersCount()
    {
        return $this->usersCount;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'usersCount' => $this->usersCount
        ];
    }

}