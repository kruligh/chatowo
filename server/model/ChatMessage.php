<?php

class ChatMessage implements JsonSerializable
{
    private $messageId;
    private $content;
    private $user;
    private $sendDate;

    public function __construct($messageId, $content, $user, $sendDate)
    {
        $this->messageId = $messageId;
        $this->content = $content;
        $this->user = $user;
        $this->sendDate = $sendDate;
    }

    function jsonSerialize()
    {
        return [
            'id' => $this->messageId,
            'content' => $this->content,
            'user' => $this->user,
            'sendDate' => $this->sendDate
        ];
    }


}