<?php

class MailBody
{
    private $toMail;
    private $subject;
    private $message;


    public function __construct($toMail, $subject, $message)
    {
        if(!isset($toMail) || !isset($subject) || !isset($message)){
            throw new InvalidArgumentException();
        }
        $this->toMail = $toMail;
        $this->subject = $subject;
        $this->message = $message;
    }


    public function getToMail()
    {
        return $this->toMail;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getMessage()
    {
        return $this->message;
    }


}