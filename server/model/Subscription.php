<?php

class Subscription implements JsonSerializable
{
    private $id;
    private $roomId;
    private $privUserId;
    private $name;
    private $addDate;

    public function __construct($id, $roomId, $userId, $name, $addDate)
    {
        $this->id = $id;
        $this->roomId = $roomId;
        $this->privUserId = $userId;
        $this->name = $name;
        $this->addDate = $addDate;
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getRoomId()
    {
        return $this->roomId;
    }

    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;
    }

    public function getPrivUserId()
    {
        return $this->privUserId;
    }

    public function setPrivUserId($privUserId)
    {
        $this->privUserId = $privUserId;
    }

    public function getName()
    {
        return $this->room_name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getAddDate()
    {
        return $this->addDate;
    }

    public function setAddDate($addDate)
    {
        $this->addDate = $addDate;
    }

    public function isPriv(){
        return isset($this->privUserId);
    }

    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'roomId' => $this->roomId,
            'privUserId' => $this->privUserId,
            'name' => $this->name,
            'add_date' => $this->addDate
        ];
    }

}