<?php

class UserFullInfoDTO  implements JsonSerializable{
    private $id;
    private $user_type;
    private $nick;
    private $sex;
    private $status;
    private $city;
    private $birthday;
    private $active;

    /**
     * UserFullInfoDTO constructor.
     * @param $id
     * @param $user_type
     * @param $nick
     * @param $sex
     * @param $status
     * @param $city
     * @param $birthday
     */
    public function __construct($id, $user_type, $nick, $sex, $status, $city, $birthday)
    {
        $this->id = $id;
        $this->user_type = $user_type;
        $this->nick = $nick;
        $this->sex = $sex;
        $this->status = $status;
        $this->city = $city;
        $this->birthday = $birthday;
    }

    public function getUserType()
    {
        return $this->user_type;
    }



    function jsonSerialize()
    {
        return [
            'id'=>$this->id,
            'userType'=> $this->user_type,
            'nick' => $this->nick,
            'sex' => $this->sex,
            'status' => $this->status,
            'city' => $this->city,
            'age'=>$this->calculateAge($this->birthday)
        ];
    }

    private function calculateAge($d){
        if(!isset($d)){
            return null;
        }else{
            $time = strtotime($d);
            $diff = date('Y-m-d') - date('Y-m-d',$time);
            return $diff;
        }
    }

}