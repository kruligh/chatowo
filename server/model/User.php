<?php
class User implements JsonSerializable {
    private $id;
    private $user_type;
private $nick;
private $active;

    function __construct($id, $user_type,$nick,$active)
    {
        $this->id = $id;
        $this->user_type = $user_type;
        $this->nick = $nick;
        $this->active = $active;
    }

    function getId(){
        return $this->id;
    }

    function getNick(){
        return $this->nick;
    }

    function isAnonyme(){
        return $this->user_type == "ANONYM";
    }

    function jsonSerialize()
    {
     return [
         'id'=>$this->id,
         'userType'=> $this->user_type,
         'nick' => $this->nick
     ];
    }

    public function isActive()
    {
        return $this->active == 1;
    }


}