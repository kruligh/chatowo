<?php

class MyUserFullInfoDTO  implements JsonSerializable{
    private $id;
    private $user_type;
    private $nick;
    private $sex;
    private $email;
    private $status;
    private $city;
    private $birthday;
    private $active;

    /**
     * UserFullInfoDTO constructor.
     * @param $id
     * @param $user_type
     * @param $nick
     * @param $sex
     * @param $email
     * @param $status
     * @param $city
     * @param $birthday
     * @param $active
     */
    public function __construct($id, $user_type, $nick, $sex, $email, $status, $city, $birthday, $active)
    {
        $this->id = $id;
        $this->user_type = $user_type;
        $this->nick = $nick;
        $this->sex = $sex;
        $this->email = $email;
        $this->status = $status;
        $this->city = $city;
        $this->birthday = $birthday;
        $this->active = $active;
    }

    function jsonSerialize()
    {
        return [
            'id'=>$this->id,
            'userType'=> $this->user_type,
            'nick' => $this->nick,
            'sex' => $this->sex,
            'email'=> $this->email,
            'status' => $this->status,
            'city' => $this->city,
            'age'=>$this->calculateAge($this->birthday),
            'birthday'=>$this->birthday,
            'active'=>$this->active
        ];
    }

    private function calculateAge($d){
        if(!isset($d)){
            return null;
        }else{
            $time = strtotime($d);
            $diff = date('Y-m-d') - date('Y-m-d',$time);
            return $diff;
        }
    }

}