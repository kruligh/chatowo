<?php

class Session{
    private $id;
    private $user_id;
    private $token;

    function __construct($id, $user_id,$token)
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->token = $token;
    }

    function getId(){
        return $this->id;
    }

    function getUserId()
    {
        return $this->user_id;
    }

    function getToken(){
        return $this->token;
    }
}