<?php

class MailSender
{

    public function sendMail($mail_body){

        require dirname(__DIR__).'/phpMailer/PHPMailerAutoload.php';
        require_once dirname(__DIR__).'/config/ActivationMailConfig.php';

        $mailer = new PHPMailer;
        $mailer->isSMTP();
        $mailer->Host = 'smtp.gmail.com';
        $mailer->Port = 587;
        $mailer->SMTPSecure = 'tls';
        $mailer->SMTPAuth = true;
        $mailer->Username = ActivationMailConfig::EMAIL;
        $mailer->Password = ActivationMailConfig::PASSWORD;
        $mailer->From = ActivationMailConfig::EMAIL;
        $mailer->FromName = ActivationMailConfig::FROM_NAME;
        $mailer->addAddress($mail_body->getToMail());
        $mailer->Subject = $mail_body->getSubject();
        $mailer->msgHTML($mail_body->getMessage());
        $mailer->CharSet = 'UTF-8';

        if(!$mailer->send()){
            throw new Exception("ERROR_SEND_MAIL");
        }

    }

    public function createActivationMail($toMail,$name, $activationToken):MailBody{

        require_once dirname(__DIR__)."/model/MailBody.php";
        require_once dirname(__DIR__)."/config/Config.php";

        $link = '"'.Config::APP_URL.'/activation/'.$activationToken.'"';
        $message = 'Witaj '.$name.' dziękujemy za rejestrację!<br/>Link aktywacyjny: <a href='.$link.'>'.$link.'</a><br/>';
        $subject = 'Link aktywacyjny';

        $resultMail = new MailBody($toMail,$subject,$message);

        return $resultMail;
    }

    public function createFailLoginBanMail($email, $nick){
        $subject = "Próba logowania";
        $message = "<h3>Witaj ".$nick."</h3><br><br>Odnotowaliśmy próbę wieloktornego zalogowania się z nieprawidłowym hasłem. Jeżeli nie wiesz kto próbował zalogować się kilka razy na Twoje konto polecamy zmienić hasło na bardzo trudne.";
        $resultMail = new MailBody($email,$subject,$message);

        return $resultMail;
    }
}