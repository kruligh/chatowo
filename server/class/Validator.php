<?php

class Validator
{

    public static function validateNick($input_nick)
    {

        if(!is_string($input_nick)){
            header("status",true,400);
            echo "INVALID_ARGUMENTS";
            die();
        }

        if(strlen($input_nick) < 4){
            header("status",true,400);
            echo "INVALID_NICK_TOO_SHORT";
            die();
        }

        if(Validator::isWithSpecialChars($input_nick)){
            header("status",true,400);
            echo "INVALID_NICK_SPECIAL_CHARS";
            die();
        }

        if(Validator::isVulgar($input_nick)){
            header("status",true,400);
            echo "INVALID_NICK_VULGAR";
            die();
        }
    }

    private static function isVulgar($input){
        //todo vulgar

        $kurwa = "kurw";
        $input = strtolower($input);

        if(strpos($input, $kurwa)){
            return true;
        }
        return false;
    }

    private static function isWithSpecialChars($input){
        //todo isWithoutSpecialChars
        $kurwa = "#";
        $input = strtolower($input);

        if(strpos($input, $kurwa)){
            return true;
        }
        return false;
    }

    public static function validateCity($input)
    {
        if(!is_string($input)){
            header("status",true,400);
            echo "INVALID_ARGUMENTS";
            die();
        }

        if(strlen($input) < 4){
            header("status",true,400);
            echo "INVALID_CITY_TOO_SHORT";
            die();
        }

        if(Validator::isWithSpecialChars($input)){
            header("status",true,400);
            echo "INVALID_CITY_SPECIAL_CHARS";
            die();
        }

        if(Validator::isVulgar($input)){
            header("status",true,400);
            echo "INVALID_CITY_VULGAR";
            die();
        }

    }

    public static function validateStatus($input)
    {
        if(!is_string($input)){
            header("status",true,400);
            echo "INVALID_ARGUMENTS";
            die();
        }

        if(strlen($input) < 4){
            header("status",true,400);
            echo "INVALID_STATUS_TOO_SHORT";
            die();
        }

        if(Validator::isWithSpecialChars($input)){
            header("status",true,400);
            echo "INVALID_STATUS_SPECIAL_CHARS";
            die();
        }

        if(Validator::isVulgar($input)){
            header("status",true,400);
            echo "INVALID_STATUS_VULGAR";
            die();
        }
    }

    public static function validateEmail($input)
    {
        if(!filter_var($input, FILTER_VALIDATE_EMAIL)) {
            header("status",true,400);
            echo "INVALID_EMAIL";
            die();
        }
    }

    public static function validateBirthDay($birthday)
    {
    }
}