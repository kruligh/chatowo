# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Chat

### How do I get set up? ###

clone
create confing files

server/config/DbConfig.php
database connection configuration

```
#!php

class DbConfig{
    const SERVER_URL = "";
    const USERNAME = "";
    const PASSWORD = "";
    const DB_NAME = "";
}
```

server/config/ActivationMailConfig.php
account configuration for gmail smtp


```
#!php

class ActivationMailConfig
{
    const EMAIL = "";
    const PASSWORD = "";
    const FROM_NAME = "";
}
```

server/config/Config.php


```
#!php

class Config
{
    const APP_URL = "";//frontend base url
}
```

create in: chat-front/src/app:

config.service.ts


```
#!typescript
import {Injectable} from '@angular/core';

@Injectable()
export class ConfigService{

    private  static  backendUrl = "http://localhost/server";
    public static getBaseUrl():string {
        return ConfigService.backendUrl;
    }

    public static buildUrl(path:string) : string {
        return ConfigService.backendUrl + "/" + path;
    }
}

```

contact: kroliczeek1@gmail.com