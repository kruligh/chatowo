import './polyfills.ts';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment.prod';
import { AppModule } from './app/';
import {Http, XHRBackend, RequestOptions} from "@angular/http";
import {CustomHttp} from "./app/util/custom-http.provider";

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
