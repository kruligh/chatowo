import { Component, OnInit } from '@angular/core';
import {UserService} from "../service/user.service";
import {LoginService} from "../service/login.service";
import {MyUser} from "../model/my-user.model";

@Component({
    selector: 'account-panel',
    templateUrl: '../view/account-panel.component.html',
    styleUrls: ['../style/app.component.css']
})
export class AccountPanelComponent{


    constructor(private loginService: LoginService, private userService: UserService) {

    }
}