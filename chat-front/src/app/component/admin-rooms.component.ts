import {Component, OnInit} from "@angular/core";
import {Room} from "../model/room.model";
import {AdminService} from "../service/admin.service";
import {Router} from "@angular/router";
import {Validator} from "../util/validator.util";

@Component({
    selector: 'admin-rooms',
    templateUrl: '../view/admin-rooms.component.html',
    styleUrls: ['../style/app.component.css', '../style/animatedButton.css']
})
export class AdminRoomsComponent implements OnInit{

    private rooms:Room[];
    private clickedRoom:Room;
    private newRoomName;
    private modalError;

    private addNewRoomName;
    private error;

    constructor(private adminService:AdminService, private router:Router){}

    ngOnInit(){
        this.updateRooms();
    }

    private updateRooms() {
        this.adminService.getRooms().subscribe(
            (res) => {
                this.rooms = res;
            },
            (err) => {
                console.info(err);
            }
        )
    }

    private addRoom(){
        let validator = new Validator();
        if(!validator.validateRoomName(this.addNewRoomName)){
            this.error = validator.getLastError();
            return;
        }

        this.adminService.addRoom(this.addNewRoomName).subscribe(
            (res)=>{},
            (err)=>{
                console.info(err);
                if(err.status == '401' || err.status == '403'){
                    this.router.navigate(['']);
                }else{
                    this.modalError = err;
                }
            },
            ()=>{
                this.updateRooms();
            }
        )
    }

    private editRoom(room:Room, modal){
        this.clickedRoom = room;
        modal.open();
        return false;
    }

    private closeModal(modal){
        modal.close();
        this.clickedRoom = null;

    }

    private onEditSubmit(modal){

        let validator = new Validator();
        if(!validator.validateRoomName(this.newRoomName)){
            this.modalError = validator.getLastError();
            return;
        }

        this.adminService.changeRoomName(this.clickedRoom.id, this.newRoomName).subscribe(
            (res)=>{},
            (err)=>{
                console.info(err);
                if(err.status == '401' || err.status == '403'){
                    this.router.navigate(['']);
                }else{
                    this.modalError = err;
                }
            },
            ()=>{
                console.info("DEF");
                this.updateRooms();
                modal.close();
                this.newRoomName ="";
                this.clickedRoom = null;
            }
        )

    }

    private onDeleteSubmit(modal){
        this.adminService.deleteRoom(this.clickedRoom.id).subscribe(
            (res)=>{},
            (err)=>{
                console.info(err);
                if(err.status == '401' || err.status == '403'){
                    this.router.navigate(['']);
                }else{
                    this.modalError = err;
                }
            },
            ()=>{
                this.updateRooms();
                modal.close();
                this.clickedRoom = null;
            }
        )
    }
}