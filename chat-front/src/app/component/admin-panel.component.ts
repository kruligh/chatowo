import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    selector: 'admin-panel',
    templateUrl: '../view/admin-panel.component.html',
    styleUrls: ['../style/app.component.css', '../style/chat.component.css']
})
export class AdminPanelComponent{

    constructor(private router:Router){}

    navigateRooms(){
        this.router.navigate(['adminPanel', { outlets: {'admin-router':['rooms'] }}]);
        return false;
    }

    navigateReports(){
        this.router.navigate(['adminPanel', { outlets: {'admin-router':['reports'] }}]);
        return false;

    }
}