import {Component, OnInit} from '@angular/core';
import {RegisterService} from '../service/register.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {Validator} from "../util/validator.util";

@Component({
    selector: 'register',
    templateUrl: '../view/register.component.html',
    styleUrls: ['../style/app.component.css', '../style/login-form.css', '../style/animatedButton.css']

})
export class RegisterComponent implements OnInit{

    nick :string;
    email: string;
    password: string;
    sex: number;
    passwordConfirm: string;
    city:string;
    birthday;
    termsAccepted:boolean=false;
    error:string;
    buttonDisabled=false;

    constructor(private registerService:RegisterService,private route:ActivatedRoute, private router:Router){}

    ngOnInit(){
       this.email = this.route.snapshot.params['login'];

        this.sex=0;
    }

    onSubmit(): void {

        let validator = new Validator();

        if(!validator.validateNick(this.nick)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validateEmail(this.email)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validatePassword(this.password, this.passwordConfirm)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validateCity(this.city)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validateBirthday(this.birthday)){
            this.error = validator.getLastError();
            return;
        }


        if(this.termsAccepted == false ){
            this.error = "Musisz akceptować regulamin!";
            return;
        }

        this.buttonDisabled = true;
        this.registerService.register(this.nick, this.email, this.password, this.city, this.birthday, this.sex).subscribe(
            (res)=>{
                console.info(res);
                this.onRegisteredSuccess();},
            (error :any)=> {
                this.error = error.text();
            },
            ()=>{
                this.buttonDisabled = false;
            }
        );
    }

    onRegisteredSuccess(){
        //todo info o tym ze zarejestrowano i trzeba potwierdzic maila
        this.router.navigate(['']);
    }

    showTerms(){

    }

}
