import {Component} from "@angular/core/src/metadata/directives";
import {UserService} from "../service/user.service";
import {Router} from "@angular/router";
import {Validator} from "../util/validator.util";

@Component({
    selector: 'change-password',
    templateUrl: '../view/change-password.component.html',
    styleUrls: ['../style/app.component.css', '../style/animatedButton.css']
})
export class ChangePasswordComponent{
    private newPassword:string;
    private newPasswordConfirm:string;
    private oldPassword;
    private error:string;

    constructor(private userService:UserService, private router: Router){}

    onSubmit(){
        let validator = new Validator();

        if(!validator.validatePassword(this.newPassword,this.newPasswordConfirm)){
            this.error = validator.getLastError();
            return;
        }

        this.userService.changePassword(this.oldPassword, this.newPassword)
            .subscribe(
                (res)=>{
                    this.router.navigate(['chat']);
                },
                (err)=>{
                    if(err.status == 400){
                        this.error = "coś poszło nie tak";
                    } else if(err.status == 403){
                        this.error = "niepoprawne stare hasło";
                    }
                }
            )
    }

    onCancel(){
        this.router.navigate(['userSettings']);
    }

}