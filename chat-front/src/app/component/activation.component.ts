import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RegisterService } from  '../service/register.service';


@Component({
    selector: 'activation',
    templateUrl: '../view/activation.component.html',
    styleUrls: ['../style/app.component.css']
})
export class ActivationComponent implements OnInit{

    public response:string;

    constructor(  private route: ActivatedRoute,
                  private router: Router,
                  private registerService: RegisterService){}

    ngOnInit(){
        if(this.route.snapshot.params['token']==null){
            this.response = "INVALID_ARGUMENT";
        }
        this.registerService.activeUser(this.route.snapshot.params['token'])
            .subscribe((res)=>{
                    console.info(res);
                    this.response = "Email został zatwierdozny, teraz mozesz się zalogować!";
                },
                (err)=>{
                    console.info(err.text());
                    this.response = "Niepoprawny token aktywacyjny. :(";
                });
    }


}
