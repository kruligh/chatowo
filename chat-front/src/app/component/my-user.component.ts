import { Component, OnInit } from '@angular/core';
import {UserService} from "../service/user.service";
import {LoginService} from "../service/login.service";
import {MyUser} from "../model/my-user.model";
import {Router} from "@angular/router";
import {Modal, ModalContent} from "ng2-modal";

@Component({
    selector: 'my-user',
    templateUrl: '../view/my-user.component.html',
    styleUrls: ['../style/app.component.css', '../style/my-user.component.css']
})
export class MyUserComponent implements OnInit{

    private myUser:MyUser;

    private newStatus:string;

    private isAdmin:boolean;

    constructor(private loginService:LoginService, private userService:UserService, private router:Router) {

    }

    ngOnInit(): void {
        if(this.loginService.isLogged()){

            this.userService.onMyUserUpdate().subscribe(
                (res)=>{

                    this.myUser = res;
                    if(this.myUser != null && this.myUser.userType === 'ADMIN'){
                        localStorage.setItem(LoginService.getAdminKey(),"1");
                    }else{
                        localStorage.removeItem(LoginService.getAdminKey());
                    }
                    this.isAdmin = UserService.isAdmin();
                },
                (err)=>{
                    // this.loginService.onLogout();
                    console.info(err.text());
                }
            )

            this.userService.getMyUser();

        }
    }

    logout(){
        this.loginService.logout().subscribe(
            (res)=>{
                this.loginService.onLogout();
                this.router.navigate(['']);
            },
            (err)=>{
                 this.loginService.onLogout();
                console.info(err.text());
            }
        );
        return false;
    }

    modalStatusOnClose(){
        this.newStatus ="";
    }

    modalStatusOnSubmit(){
        this.userService.setNewStatus(this.newStatus).subscribe(
            (res)=>{
                this.userService.getMyUser();
            },
            (err)=>{
                //todo
                console.info(err);
            },
            ()=>{
                this.newStatus = "";
            })

    }

    goToMain(){
        this.router.navigate(['chat']);
        return false;
    }

    goToUserSettings(){
        this.router.navigate(['userSettings']);
        return false;
    }

    goToAdminPanel(){
        this.router.navigate(['adminPanel', { outlets: {'admin-router':['rooms'] }}]);
        return false;
    }
}