import {Component} from "@angular/core/src/metadata/directives";
import {OnInit} from "@angular/core";
import {UserService} from "../service/user.service";
import {MyUser} from "../model/my-user.model";
import {Router} from "@angular/router";
import {Validator} from "../util/validator.util";

@Component({
    selector: 'user-settings',
    templateUrl: '../view/user-settings.component.html',
    styleUrls: ['../style/app.component.css', '../style/animatedButton.css']
})
export class UserSettingsComponent implements OnInit{

    private myUser:MyUser;
    private error:string;

    constructor(private userService:UserService, private router: Router){}

    ngOnInit(): void {

    this.myUser = this.userService.getMyUserStatic();
    }

    goToChangePassword(){
        this.router.navigate(['changePassword']);
        return false;
    }

    onSubmit(){

        let validator = new Validator();

        if(!validator.validateNick(this.myUser.nick)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validateEmail(this.myUser.email)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validateCity(this.myUser.city)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validateBirthday(this.myUser.birthday)){
            this.error = validator.getLastError();
            return;
        }

        this.userService.updateUser(this.myUser)
            .subscribe(
                (res)=>{
                    this.userService.getMyUser();
                    this.router.navigate(['chat']);
                },
                (err)=>{
                    console.info(err);
                    //todo
                }
            )
    }

    onCancel(){
        this.router.navigate(['chat']);
    }
}