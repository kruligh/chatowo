
import {Component} from "@angular/core/src/metadata/directives";
import {ChatService} from "../service/chat.service";
import {OnInit} from "@angular/core";
import {Room} from "../model/room.model";
import {Router, NavigationStart} from "@angular/router";
@Component({
    selector: 'rooms',
    templateUrl: '../view/rooms.component.html',
    styleUrls: ['../style/app.component.css', '../style/chat.component.css']
})
export class RoomsComponent implements OnInit{

    private rooms:Room[];
    private error:string;

    constructor(private chatService: ChatService, private router :Router){
        router.events.subscribe(event => {
            if(event instanceof NavigationStart && (<NavigationStart> event).url === "/chat") {
                this.chatService.getAllRooms();
            }
        });
    }

    ngOnInit(){
        this.getRooms();
    }

    private getRooms() {
        this.chatService.onAllRoomsRefresh().subscribe(
            (res)=>{this.rooms = res;},
            (err)=>{
                console.info(err);
            this.error = err;}
        )
    }

    select(room){
        this.chatService.subscribeRoom(room.id);
        return false;
    }
}