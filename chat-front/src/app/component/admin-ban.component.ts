import {Component, OnInit} from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import {AdminService} from "../service/admin.service";


@Component({
    selector: 'admin-ban',
    templateUrl: '../view/admin-ban.component.html',
    styleUrls: ['../style/app.component.css']

})
export class AdminBanComponent implements OnInit {

    private userId;
    private userNick;
    private reportId;
    private reason;
    private end;

    constructor(private adminService:AdminService, private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit(): void {
        this.userId = this.route.snapshot.params['userId'];
        this.userNick = this.route.snapshot.params['userNick'];
        this.reportId = this.route.snapshot.params['reportId'];
    }

    onSubmit(){
        this.adminService.banUser(this.userId, this.reason, this.end)
            .subscribe(
                (res)=>{
                    this.router.navigate(['chat']);
                },
                (err)=>{
                    console.info(err);
                }
            )
    }

}