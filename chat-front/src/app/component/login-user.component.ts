import { Component, OnInit } from '@angular/core';
import {LoginService} from '../service/login.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {Validator} from "../util/validator.util";


@Component({
    selector: 'login-user',
    templateUrl: '../view/login-user.component.html',
    styleUrls: ['../style/app.component.css', '../style/login-form.css', '../style/animatedButton.css']
})

export class LoginUserComponent implements OnInit{

    login :string ;
    password: string;
    error:string;

    constructor(private loginService:LoginService,
                private route: ActivatedRoute,
                private router:Router){}

    ngOnInit(){
        this.error = this.route.snapshot.params['message'];
    }

    onSubmit(): void {
        let validator = new Validator();

        if(!validator.validateLogin(this.login)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validateLoginPassword(this.password)){
            this.error = validator.getLastError();
            return;
        }

        this.loginService.loginUser(this.login,this.password).subscribe(
            (res)=>{
                console.info(res.text());
                this.loginService.onLogged();
                this.router.navigate(['chat']);
            },
            (err)=>{
                this.error = err.text();
                console.info(this.error);
            }
        );
    }

    goRegister(){
        if(this.login){
            this.router.navigate(['register', {login: this.login}]);
        }else{
            this.router.navigate(['register']);
        }

        return false;
    }

    goForgotPassword(){
        this.router.navigate(['forgotPassword']);
        return false;
    }

}
