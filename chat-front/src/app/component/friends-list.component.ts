import {Component, Input, OnChanges, SimpleChanges, OnInit} from "@angular/core";
import {Friend} from "../model/friend.model";
import {ChatService} from "../service/chat.service";
import {Router, NavigationStart} from "@angular/router";

@Component({
    selector: 'friends-list',
    templateUrl: '../view/friends-list.component.html',
    styleUrls: ['../style/app.component.css', '../style/chat.component.css']
})
export class FriendsListComponent implements OnInit{

    private friends:Friend[];
    private error:string;

    constructor(private chatService: ChatService, private router:Router){
        router.events.subscribe(event => {
            if(event instanceof NavigationStart && (<NavigationStart> event).url === "/chat") {
                this.chatService.getFriends();
            }
        });
    }

    ngOnInit(){
        this.chatService.onNewFriend().subscribe(
            (res)=>{
                this.friends = res;
            }
        );
        this.chatService.getFriends();
    }

    select(friend:Friend){
        this.chatService.startPriv(friend.id).subscribe(
            ()=>{
                this.chatService.getSubscribedRooms();
            },
            (err)=>{
                console.info(err);
            }
        );
        return false;
    }

    deleteFriend(friend:Friend){
        this.chatService.deleteFriend(friend.id).subscribe(
            ()=>{
                this.chatService.getFriends();
            },
            (err)=>{
                console.info(err);
            }
        );
        return false;
    }
}