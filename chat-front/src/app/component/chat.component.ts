import {Component, Input, DoCheck, OnDestroy, OnInit} from "@angular/core";
import {ChatService} from "../service/chat.service";
import {Conversation} from "../model/conversation.model";

import {Router, NavigationStart, NavigationEnd} from "@angular/router";
@Component({
    selector: 'chat',
    templateUrl: '../view/chat.component.html',
    styleUrls: ['../style/app.component.css', '../style/chat.component.css']
})
export class ChatComponent implements OnInit{

    private openedConversations:Conversation[]= new Array();
    private selectedConversation:Conversation;

    constructor (private chatService: ChatService, private router:Router){
        router.events.subscribe(event => {
            if(event instanceof NavigationStart && (<NavigationStart> event).url === "/chat") {
                this.chatService.getSubscribedRooms();
            }
        });
    }

    ngOnInit(): void {
        this.chatService.startChecking();
        this.chatService.onSubscribeRoom().subscribe(rooms=> {
                for(let room of rooms){
                    if(!room.privUserId){
                        if(!this.containsById(this.openedConversations,room)){
                            this.openedConversations.push(room);
                            this.selectedConversation = room;
                        }
                    }else{
                        if(!this.containsByPrivUserId(this.openedConversations,room)){
                            this.openedConversations.push(room);
                            this.selectedConversation = room;
                        }
                    }
                }

               this.openedConversations = this.openedConversations.filter((openedRoom)=>{
                   return this.containsById(rooms,openedRoom) || this.containsByPrivUserId(rooms,openedRoom) ;
               })
            },
            (err)=>{
                console.info(err);
            });
    }

    private containsById(array,searchFor){
        for(let item of array) {
            if (item.id === searchFor.id) {
                return true;
            }
        }
        return false;
    }

    private containsByPrivUserId(array,searchFor){
        for(let item of array) {
            if (item.privUserId!= null && item.privUserId === searchFor.privUserId) {
                return true;
            }
        }
        return false;
    }

    select(selected:Conversation){
        this.selectedConversation = selected;
        return false;
    }

    unSubscribeRoom(selected: Conversation){

        this.chatService.unSubscribe(selected.id).subscribe(
            (res)=>{
                this.chatService.getSubscribedRooms();
            },
            (err)=>{
                console.info(err);
            },
            ()=>{
                if(selected==this.selectedConversation){
                    this.selectedConversation = null;
                }
            }
        )
    }


}