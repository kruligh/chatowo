import {Component, OnInit, DoCheck} from '@angular/core';
import {LoginService} from "../service/login.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: '../view/app.component.html',
  styleUrls: ['../style/app.component.css']
})
export class AppComponent{

  constructor(private loginService: LoginService,
              private router: Router) {
  }

}
