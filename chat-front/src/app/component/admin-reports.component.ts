import {Component, OnInit} from "@angular/core";
import {AdminService} from "../service/admin.service";
import {Router} from "@angular/router";
import {Report} from "../model/report.model";

@Component({
    selector: 'admin-rooms',
    templateUrl: '../view/admin-reports.component.html',
    styleUrls: ['../style/app.component.css', '../style/animatedButton.css']
})
export class AdminReportsComponent implements OnInit{

    private reports:Report[];
    private clickedReport;
    private banReason;
    private modalError;
    private endBanDate;

    constructor(private adminService:AdminService, private router:Router){}

    ngOnInit(){
        this.getNewReports();
    }

    private getNewReports() {
        this.adminService.getReports().subscribe(
            (res) => {
                this.reports = res;
            },
            (err) => {
                console.info(err);
            }
        )
    }

    private banUser(report: Report,modal){
        this.clickedReport = report;
        this.banReason = report.reason;
        modal.open();
        return false;
    }

    onBanSubmit(report:Report,modal){

        this.adminService.banUser(report.user.id,report.reason, this.endBanDate, report.id)
            .subscribe(
                (res)=>{
                    this.getNewReports();
                },
                (err)=>{
                    this.modalError = err;
                    console.info(err);
                },
                ()=>{
                    this.closeModal(modal);
                }
            )
    }

    private closeModal(modal){
        modal.close();
        this.banReason = "";
        this.clickedReport = null;
    }

    private consumeReport(report: Report){
        this.adminService.consumeReport(report.id).subscribe(
            ()=>{
                this.getNewReports();
            },
            (err)=>{
                console.info(err);
            }
        );
        return false;
    }
}
