import { Component } from '@angular/core';
import {LoginService} from "../service/login.service";
import {Router} from "@angular/router";
import {Validator} from "../util/validator.util";

@Component({
    selector: 'login-anonym',
    templateUrl: '../view/login-anonym.component.html',
    styleUrls: ['../style/app.component.css', '../style/login-form.css', '../style/animatedButton.css']
})
export class LoginAnonymComponent {
    nick:string;
    city:string;
    birthday;
    sex:number;
    error:string;
    termsAccepted:boolean = false;

    constructor(private loginService:LoginService, private router:Router){
        this.sex=0;
    }

    onSubmit(){
        let validator = new Validator();

        console.info(this.birthday);

        if(!validator.validateNick(this.nick)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validateCity(this.city)){
            this.error = validator.getLastError();
            return;
        }

        if(!validator.validateBirthday(this.birthday)){
            this.error = validator.getLastError();
            return;
        }

        if(this.termsAccepted == false ){
            this.error = "Musisz akceptować regulamin!";
            return;
        }

        this.loginService.loginAnonym(this.nick,this.city, this.birthday, this.sex).subscribe(
            (res)=>{
                this.loginService.onLogged();
                this.router.navigate(['chat']);
            },
            (err)=>{
                this.error = err.text();
                console.info(err);
            }
        )

    }
}
