import {Component, Input, OnChanges, SimpleChanges, OnInit} from "@angular/core";
import {ChatService} from "../service/chat.service";
import {Room} from "../model/room.model";
import {ChatMessage} from "../model/chat-message.model";
import {Conversation} from "../model/conversation.model";
import {User} from "../model/user.model";
import {forEach} from "@angular/router/src/utils/collection";
import {RoomMessagesDto} from "../model/room-messages.dto";
import {FullUser} from "../model/full-user.model";
import {UserService} from "../service/user.service";
import {Subscription} from "rxjs";
import {LoginService} from "../service/login.service";
import {Router} from "@angular/router";
import {Validator} from "../util/validator.util";
@Component({
    selector: 'room',
    templateUrl: '../view/room.component.html',
    styleUrls: ['../style/app.component.css', '../style/animatedButton.css', '../style/chat.component.css']
})
export class RoomComponent implements OnInit{

    @Input() conversation:Conversation;
    newMessage:string;
    private messages:ChatMessage[];
    private hoveredUser:FullUser;
    private modalError:string;
    private clickedMessage:ChatMessage;
    private getUserInfoSubscription:Subscription;
    private isAdmin:boolean;
    private reportReason:string;
    private buttonDisabled = false;

    constructor(private chatService: ChatService, private userService:UserService, private router:Router) {
        this.messages = new Array();
    }

    ngOnInit(){
        this.isAdmin = UserService.isAdmin();

        if(this.conversation.privUserId){
            this.chatService.onMessage().subscribe((roomMessagesArray:RoomMessagesDto[]) => {
                    for(var roomMessages of roomMessagesArray){
                        if(this.conversation.privUserId == roomMessages.privUserId){
                            this.conversation.newMessagesCount = roomMessages.messages.length - this.messages.length;
                            this.messages=roomMessages.messages;
                        }
                    }
                },
                (err)=>{
                    console.info(err);
                });

        }else{
            this.chatService.onMessage().subscribe((roomMessagesArray:RoomMessagesDto[]) => {
                    for(var roomMessages of roomMessagesArray){
                        if(this.conversation.roomId == roomMessages.roomId){
                            this.conversation.newMessagesCount = roomMessages.messages.length - this.messages.length;
                            this.messages=roomMessages.messages;
                        }
                    }
                },
                (err)=>{
                    console.info(err);
                });
        }
        this.chatService.getMessages();
    }

    sendMessage(){

        if(!this.newMessage || 0 === this.newMessage.trim().length){
            return;
        }

        this.buttonDisabled = true;
            this.chatService.sendMessage(this.conversation, this.newMessage)
                .subscribe((res)=>{
                        this.chatService.getMessages();
                    },
                    (err)=>{
                       console.info(err);
                    },
                    ()=>{
                        this.buttonDisabled = false;
                    });
        this.newMessage="";
    }


    mouseOver(msg){

        if(this.getUserInfoSubscription != null){
            this.getUserInfoSubscription.unsubscribe();
        }
        this.getUserInfoSubscription = this.userService.getUserInfo(msg.user.id).subscribe(
                (res)=>{
                    this.hoveredUser=res;
                },
                (err)=>{
                    //todo if 403
                    console.info(err);
                    //todo error show
                }
            );
    }

    private closeModal(modal){
        modal.close();
        this.clickedMessage = null;
        this.reportReason = "";

    }

    mouseLeave(){
        this.hoveredUser = null;
        if(this.getUserInfoSubscription != null){
            this.getUserInfoSubscription.unsubscribe();
            this.getUserInfoSubscription = null;
        }
    }

    onMessageClick(message:ChatMessage, userInteractionModal){
        if(message.user.id == this.userService.getMyUserStatic().id){
            this.clickedMessage = null;
        }else{
            userInteractionModal.open();
            this.clickedMessage = message;
        }

    }

    goPriv(user:User, modal){
        this.chatService.startPriv(user.id).subscribe((res)=>{
            this.chatService.getSubscribedRooms();
            },
            (err)=>{
                this.modalError = "wystąpił błąd :(";
                console.info(err);
            },()=>{
                this.closeModal(modal);
            });
    }

    reportMessage(modalOld, modalNew){
        modalOld.close();
        modalNew.open();
    }

    onReportSubmit(msg:ChatMessage, modal){
        let validator = new Validator();
        if(!validator.validateReason(this.reportReason)){
            this.modalError = validator.getLastError();
            return;
        }

        let msgId= null, privMsgId = null;
        if(this.conversation.roomId == null){
            privMsgId = msg.id;
        }else{
            msgId = msg.id;
        }
        this.chatService.reportMessage(msgId, privMsgId, this.reportReason).subscribe(
            (res)=>{
                this.closeModal(modal);
            },
            (err)=>{
                this.modalError = "Coś poszło nie tak";
                console.info(err);
            }
        )
    }

    addFriend(user: User, modal){
        this.chatService.addFriend(user.id).subscribe((res)=>{
                this.chatService.getFriends();
            },
            (err)=>{
                if(err.status == 401){
                    this.modalError = "Załóż konto aby zapraszać znajomych Panie Anonimie ;)";
                }else if(err.status == 409){
                    this.modalError = "Użytkownik jest już Twoim znajomym";
                }

                console.info(err);
            },()=>{
                this.closeModal(modal);
            });
    }

    banUser(user: User, modal){
        this.closeModal(modal);
        this.router.navigate(['banUser',{userId: user.id, userNick: user.nick}]);
    }
}