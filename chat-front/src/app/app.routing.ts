import { Routes, RouterModule} from '@angular/router';
import { ModuleWithProviders }  from '@angular/core';
import {MainComponent} from "./component/main.component";
import {LoginComponent} from "./component/login.component";
import {LoggedInGuard} from "./guard/logged-in.guard";
import {RegisterComponent} from "./component/register.component";
import {ActivationComponent} from "./component/activation.component";
import {WelcomeComponent} from "./component/welcome.component";
import {UserSettingsComponent} from "./component/user-settings.component";
import {LoggedOutGuard} from "./guard/logged-out.guard";
import {IsAdminGuard} from "./guard/is-admin.guard";
import {AdminBanComponent} from "./component/admin-ban.component";
import {AdminPanelComponent} from "./component/admin-panel.component";
import {AdminRoomsComponent} from "./component/admin-rooms.component";
import {AdminReportsComponent} from "./component/admin-reports.component";
import {ChangePasswordComponent} from "./component/change-password.component";

const appRoutes: Routes = <Routes>[

    {
        path: '',
        component: WelcomeComponent,
        canActivate: [LoggedOutGuard]
    },
    {
        path:'register',
        component: RegisterComponent,
        canActivate: [LoggedOutGuard]
    },
    {
        path: 'activation/:token',
        component: ActivationComponent,
        canActivate: [LoggedOutGuard]
    },
    {
        path:'chat',
        component: MainComponent,
        canActivate: [LoggedInGuard]
    },
    {
        path:'userSettings',
        component: UserSettingsComponent,
        canActivate: [LoggedInGuard]
    },
    {
        path:'changePassword',
        component: ChangePasswordComponent,
        canActivate: [LoggedInGuard]
    },
    {
        path:'banUser',
        component: AdminBanComponent,
        canActivate: [IsAdminGuard]
    },
    {
        path:'adminPanel',
        component: AdminPanelComponent,
        canActivate: [IsAdminGuard],
        children: [
            { path: 'rooms', component: AdminRoomsComponent, outlet:'admin-router'},
            { path: 'reports', component: AdminReportsComponent, outlet:'admin-router'}
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
