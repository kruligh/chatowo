import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpModule, Http, XHRBackend, RequestOptions} from '@angular/http';
import { routing } from './app.routing';

import { AppComponent } from './component/app.component';
import { LoginAnonymComponent} from  './component/login-anonym.component';
import {ConfigService} from "./config.service";
import {LoginService} from "./service/login.service";
import {LoggedInGuard} from "./guard/logged-in.guard";
import {LoginComponent} from "./component/login.component";
import {MainComponent} from "./component/main.component";
import {RoomsComponent} from "./component/rooms.component";
import {ChatService} from "./service/chat.service";
import {ChatComponent} from "./component/chat.component";
import {RoomComponent} from "./component/room.component";
import {LoginUserComponent} from "./component/login-user.component";
import {RegisterComponent} from "./component/register.component";
import {RegisterService} from "./service/register.service";
import {ActivationComponent} from "./component/activation.component";
import {MyUserComponent} from "./component/my-user.component";
import {UserService} from "./service/user.service";
import {AccountPanelComponent} from "./component/account-panel.component";
import {WelcomeComponent} from "./component/welcome.component";
import {PopoverModule} from "ng2-popover";
import {ModalModule} from "ng2-modal";
import {UserSettingsComponent} from "./component/user-settings.component";
import {FriendsListComponent} from "./component/friends-list.component";
import {LoggedOutGuard} from "./guard/logged-out.guard";
import {CookieService} from 'angular2-cookie/core';
import {CustomHttp} from "./util/custom-http.provider";
import {AdminBanComponent} from "./component/admin-ban.component";
import {IsAdminGuard} from "./guard/is-admin.guard";
import {AdminService} from "./service/admin.service";
import {AdminPanelComponent} from "./component/admin-panel.component";
import {AdminRoomsComponent} from "./component/admin-rooms.component";
import {AdminReportsComponent} from "./component/admin-reports.component";
import {ChangePasswordComponent} from "./component/change-password.component";
import { CookieLawModule } from 'angular2-cookie-law';

@NgModule({
  declarations: [
    AppComponent,
      RegisterComponent,
      ActivationComponent,
    LoginComponent,
    LoginAnonymComponent,
      LoginUserComponent,
      MainComponent,
      ChatComponent,
      RoomsComponent,
      FriendsListComponent,
      RoomComponent,
      MyUserComponent,
      AccountPanelComponent,
      WelcomeComponent,
      UserSettingsComponent,
      AdminBanComponent,
      AdminPanelComponent,
      AdminRoomsComponent,
      AdminReportsComponent,
      ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CookieLawModule,
    HttpModule,
    routing,
      PopoverModule,
      ModalModule
  ],
  providers: [
    ConfigService,
      RegisterService,
    LoginService,
      ChatService,
    LoggedInGuard,
    LoggedOutGuard,
      IsAdminGuard,
      UserService,
      AdminService,
    CookieService,
    { provide: Http,
      useFactory: (
          backend: XHRBackend,
          defaultOptions: RequestOptions) =>
          new CustomHttp(backend, defaultOptions),
      deps: [XHRBackend, RequestOptions]
    }
  ],
  bootstrap: [
      AppComponent
  ]
})
export class AppModule {
}
