import {Injectable} from "@angular/core";
import {Http, ConnectionBackend, Request, Response, RequestOptionsArgs, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";

@Injectable()
export class CustomHttp extends Http {


    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);

    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
       // console.info(Date.now() + "GET " + url);
        options.withCredentials = true;
        return super.get(url, options).map((res)=>{

            return res;
        });
    }


    post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
       console.info(Date.now() + "POST " + url + "\n" + body);
        options.withCredentials = true;
        return super.post(url, body, options).map((res)=>{
            return res;
        });
    }
}