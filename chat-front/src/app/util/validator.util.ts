import {DatePipe} from "@angular/common";
export class Validator{

    error:string;

    validateNick(nick:string):boolean{
        this.error="";
        if(nick == null ){
            this.error = "Podaj nick!";
            return false;
        }else if(nick == "" ){
            this.error = "Podaj nick!";
            return false;
        } else if(nick.length<4 ){
            this.error = "Zbyt krótki nick (4-32)";
            return false;
        }else if(nick.length>32 ){
            this.error = "Zbyt długi nick (4-32)";
            return false;
        }else{
            return true;
        }
    }

    validatePassword(password: string, passwordConfirm:string) {
        this.error = "";
        if (password == null) {
            this.error = "Podaj hasło!";
            return false;
        } else if (password == "") {
            this.error = "Podaj hasło!";
            return false;
        } else if (password.length < 6) {
            this.error = "Zbyt krótkie hasło (6-32)";
            return false;
        } else if (password.length > 32) {
            this.error = "Zbyt długie hasło (6-32)";
            return false;
        } else if (password.indexOf('`')>=0){
            this.error = "W haśle nie możesz użyć `";
            return false;
        }else if(password!==passwordConfirm){
        this.error = "Potwierdź hasło";
        return false;
        }else{
            return true;
        }
    }

    validateEmail(email: string) {
        this.error="";

        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

        if(email == null ){
            this.error = "Podaj email!";
            return false;
        }else if(email == "" ){
            this.error = "Podaj email!";
            return false;
        }else if (!EMAIL_REGEXP.test(email)) {
            this.error = "Podaj poprawny email!";
            return false;
        } else{
            return true;
        }
    }

    getLastError(){
        return this.error;
    }

    validateCity(city: string) {
        this.error="";
        if(city == null ){
            this.error = "Podaj miasto!";
            return false;
        }else if(city == "" ){
            this.error = "Podaj miasto!";
            return false;
        }else{
            return true;
        }
    }

    validateBirthday(birthday: any) {
        this.error="";
        if(birthday==null || birthday == ""){
            this.error = "Podaj datę urodzenia!";
            return false;
        }else{

            let dateBday = Date.parse(birthday);
            console.info(dateBday);
            if(isNaN(dateBday)){
                this.error = "niepoprawny format yyyy-mm-dd :)";
                return false;
            }
            else if(dateBday>Date.now()){
                this.error = "serio? zbyt młody jesteś :)";
                return false;
            }else if(dateBday<Date.parse("1900-01-01")){
                this.error = "serio? to czat dla żywych :)";
                return false;
            }
            return true;
        }

    }

    validateLogin(login: string) {
        this.error="";
        if(login == null ){
            this.error = "Podaj nick!";
            return false;
        }else if(login == "" ){
            this.error = "Podaj nick!";
            return false;
        }else{
            return true;
        }
    }

    validateLoginPassword(password: string) {
        this.error="";
        if(password == null ){
            this.error = "Podaj haslo!";
            return false;
        }else if(password == "" ){
            this.error = "Podaj haslo!";
            return false;
        }else{
            return true;
        }
    }

    validateRoomName(name:string) {
        this.error ="";
        if(name == null ){
            this.error = "Podaj nazwe!";
            return false;
        }else if(name == "" ){
            this.error = "Podaj nazwe!";
            return false;
        } else if(name.length<4 ){
            this.error = "Zbyt krótka nazwa (4-32)";
            return false;
        }else{
            return true;
        }
    }

    validateReason(reason: string) {
        this.error ="";
        if(reason == null ){
            this.error = "Podaj powód!";
            return false;
        }else if(reason == "" ){
            this.error = "Podaj powód!";
            return false;
        } else if(reason.length<8 ){
            this.error = "zbyt krótki powód ";
            return false;
        }else{
            return true;
        }
    }
}