import {Injectable} from "@angular/core";
import {Response, Http, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import {ConfigService} from "../config.service";
import {Room} from "../model/room.model";
import {Report} from "../model/report.model";


@Injectable()
export class AdminService{

    constructor(private http:Http) {}

    banUser(userId, reason, end, reportId = null): Observable<Response> {
        let body = JSON.stringify({reportId, userId , reason, end});
        console.info(body);
        let url = ConfigService.buildUrl('admin_ban.php');
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url,body,options);
    }

    getRooms():Observable<Room[]> {
        let url = ConfigService.buildUrl("get_all_rooms.php");
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers,  body: "" });
        return this.http.get(url,options)
            .map(res => res.json());
    }

    changeRoomName(roomId: number, newRoomName: any) {
        let url = ConfigService.buildUrl("admin_update_room_name.php");
        let body = JSON.stringify({ roomId , newRoomName});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url,body,options);
    }

    deleteRoom(roomId: number) {
        let url = ConfigService.buildUrl("admin_delete_room.php");
        let body = JSON.stringify({ roomId});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url,body,options);
    }

    addRoom(roomName: string) {
        let url = ConfigService.buildUrl("admin_add_room.php");
        let body = JSON.stringify({ roomName});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url,body,options);
    }


    getReports():Observable<Report[]> {
        let url = ConfigService.buildUrl("admin_get_reports.php");
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers,  body: "" });
        return this.http.get(url,options)
            .map(res => res.json());
    }

    consumeReport(reportId: number) {
        let body = JSON.stringify({ reportId});
        console.info(body);
        let url = ConfigService.buildUrl('admin_report_consume.php');
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url,body,options);
    }
}