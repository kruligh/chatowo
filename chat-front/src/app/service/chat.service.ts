import {Injectable, OnInit, AfterContentInit} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable, Subject, Observer, Subscription} from 'rxjs/Rx';
import {ConfigService} from '../config.service';
import {Room} from "../model/room.model";
import {LoginService} from "./login.service";
import {Conversation} from "../model/conversation.model";
import {RoomMessagesDto} from "../model/room-messages.dto";
import {Friend} from "../model/friend.model";
import {Router} from "@angular/router";
import {ChatMessage} from "../model/chat-message.model";
import {User} from "../model/user.model";


@Injectable()
export class ChatService{

    private subscribedRooms: Subject<Conversation[]>;
    private allRooms: Subject<Room[]>;
    private friends: Subject<Friend[]>;
    private messages: Subject<RoomMessagesDto[]>;
    private privMessages: Subject<RoomMessagesDto[]>;
    private error:string;
    private timerMessage;
    private timerAllRooms;
    private timerSubscribedRooms;
    private subscriptionAllRooms:Subscription;
    private subscriptionMessage:Subscription;
    private subscriptionSubscribedRooms:Subscription;

    constructor(private http:Http, private router:Router) {
        this.subscribedRooms = new Subject<Conversation[]>();
        this.allRooms = new Subject<Room[]>();
        this.friends = new Subject<Friend[]>();
        this.messages = new Subject<RoomMessagesDto[]>();
        this.privMessages = new Subject<RoomMessagesDto[]>();
        this.timerSubscribedRooms = Observable.timer(0,10000);
        this.timerAllRooms = Observable.timer(0,5000);
        this.timerMessage = Observable.timer(1000,2000);
    }

    public startChecking(){
        console.info("startsubscribe");
        if(!this.subscriptionSubscribedRooms) {
            this.subscriptionSubscribedRooms = this.timerSubscribedRooms.subscribe(t=> {
                this.getSubscribedRooms();
            });
        }

        if(!this.subscriptionAllRooms) {
            this.subscriptionAllRooms = this.timerAllRooms.subscribe(t=> {
                this.getAllRooms();
            });
        }

        if(!this.subscriptionMessage) {
            this.subscriptionMessage = this.timerMessage.subscribe(t=> {
                this.getMessages();
            });
        }
    }

    public stopChecking(){
        console.info("stopsubscribe");
        if(this.subscriptionSubscribedRooms){
            this.subscriptionSubscribedRooms.unsubscribe();
            this.subscriptionSubscribedRooms = null;
        }
        if(this.subscriptionMessage){
            this.subscriptionMessage.unsubscribe();
            this.subscriptionMessage = null;
        }
        if(this.subscriptionAllRooms){
            this.subscriptionAllRooms.unsubscribe();
            this.subscriptionAllRooms = null;
        }
    }

    clear(): void {
       // this.subscribedRooms = null;
    }

    getAllRooms(){
        let url = ConfigService.buildUrl("get_all_rooms.php");
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers,  body: "" });
        return this.http.get(url,options)
            .map(this.extractData)
            .subscribe(
                (res:Room[])=>{
                    this.allRooms.next(res);
                },
                (err)=>{
                    if(err.status == 403){
                        this.on403Error();
                    }
                    console.info(err);
                }
            );
    }

    private extractData(res: Response){
        let body = res.json();
        return body || { };
    }

    subscribeRoom(id: number) {
        let url = ConfigService.buildUrl("subscribe_room.php");
        let body = JSON.stringify({id});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url,body,options)
            .subscribe(
                (res)=>{
                    this.getSubscribedRooms();
                },
                (err)=>{
                    this.error = err.text();}
            );
    }

    unSubscribe(subscriptionId: number){
        console.info(subscriptionId);
        let url = ConfigService.buildUrl("un_subscribe_room.php");
        let body = JSON.stringify({subscriptionId});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url,body,options);
    }

    public getSubscribedRooms() {
        let url = ConfigService.buildUrl("get_subscribed.php");
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers,  body: ""});
        return this.http.get(url,options)
            .map(this.extractData)
            .subscribe(
                (res)=>{this.subscribedRooms.next(res);
                },
                (err)=>{
                    if(err.status == 403){
                        this.stopChecking();
                    }
                    console.info(err);
                }
            )
    }

    getFriends() {
        let url = ConfigService.buildUrl("get_friends.php");
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers,  body: ""});
        return this.http.get(url,options)
            .map(this.extractData)
            .subscribe(
                (res)=>{
                    console.info("f" + res);
                    this.friends.next(res);
                },
                (err)=>{console.info(err);this.error =err;}
            )
    }

    public sendMessage(conversation:Conversation, content:string){
        console.info(conversation);
        if(conversation.privUserId){
            console.info("SENDPRIV");
            return this.sendPrivMessage(conversation.privUserId,content);
        }else{
            console.info("SENDPUBLIC");
            return this.sendPublicMessage(conversation.roomId,content);
        }
    }

    private sendPublicMessage(roomId: number, content: string){
        let url = ConfigService.buildUrl("send_message.php");
        let body = JSON.stringify({roomId, content});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers});
        return this.http.post(url,body,options);
    }

    private sendPrivMessage(toUserId: number, content: string){
        let url = ConfigService.buildUrl("send_priv_message.php");
        let body = JSON.stringify({toUserId, content});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers});
        return this.http.post(url,body,options);
    }

    getMessages() {
        let url = ConfigService.buildUrl("get_messages.php");
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers,  body: ""});
        return this.http.get(url,options)
            .map(this.extractData)
            .subscribe(
                (res:RoomMessagesDto[])=>{
                    this.messages.next(res);
                },
                (err)=>{
                    if(err.status == 403){
                        this.on403Error();
                    }
                    console.info(err);
                }
            )


    }

    private on403Error() {
        this.stopChecking();

        localStorage.removeItem(LoginService.getLoggedKey());
        this.router.navigate(['']);
    }

    public onMessage(): Observable<RoomMessagesDto[]> {
        return this.messages.asObservable();
    }

    public onAllRoomsRefresh(): Observable<Room[]> {
        return this.allRooms.asObservable();
    }

    public onSubscribeRoom():Observable<Conversation[]>{
        return this.subscribedRooms.asObservable();
    }

    public onNewFriend():Observable<Friend[]>{
        return this.friends.asObservable();
    }

    startPriv(userId) {
        let url = ConfigService.buildUrl("start_priv.php");

        let body = JSON.stringify({userId});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers});
        return this.http.post(url,body,options);

    }

    addFriend(userId) {
        let url = ConfigService.buildUrl("friend_add.php");
        let body = JSON.stringify({userId});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers});
        return this.http.post(url,body,options);
    }

    deleteFriend(userId: number) {
        let url = ConfigService.buildUrl("friend_delete.php");
        let body = JSON.stringify({userId});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers});
        return this.http.post(url,body,options);
    }

    reportMessage(messageId, privMessageId, reason) {
        let url = ConfigService.buildUrl("report_message.php");
        let body = JSON.stringify({messageId, privMessageId, reason});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers});
        return this.http.post(url,body,options);
    }

}