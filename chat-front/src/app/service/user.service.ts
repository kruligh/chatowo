import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable, Subject, BehaviorSubject} from 'rxjs/Rx';
import {MyUser} from "../model/my-user.model";
import {ConfigService} from "../config.service";
import {LoginService} from "./login.service";
import {hash} from "../util/sha1";


@Injectable()
export class UserService{

    private myUser:BehaviorSubject<MyUser>;

    constructor(private http:Http, private loginService:LoginService) {
        this.myUser = new BehaviorSubject<MyUser>(null);
    }

    getMyUser() {
        let url = ConfigService.buildUrl("get_my_user.php");
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers,  body: ""});

        return this.http.get(url,options)
            .map((res)=>{console.info(res);return res.json(); })
            .subscribe((res)=>{
                this.myUser.next(res);
            },
                (err)=>{
                    console.info(err);
                    this.loginService.onLogout();
                });
    }

    setNewStatus(newStatus: string) {
        let url = ConfigService.buildUrl("set_status.php");
        let body = JSON.stringify({newStatus});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url,body,options);
    }

    getUserInfo(id: number) {
        let url = ConfigService.buildUrl("get_user_info.php?id="+id);
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers,  body: ""});
        return this.http.get(url,options)
            .map((res)=>{return res.json(); });
    }

    updateUser(myUser: MyUser):Observable<Response> {
        let url = ConfigService.buildUrl("update_user.php");
        let body = JSON.stringify(myUser);
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers});
        console.info(body);
        return this.http.post(url,body,options);
    }

    onMyUserUpdate(): Observable<MyUser>{
        return this.myUser.asObservable();
    }

    getMyUserStatic(){
        return this.myUser.getValue();
    }

    static isAdmin():boolean{
        if(localStorage.getItem(LoginService.getAdminKey())){
            return true;
        }else{
            return false;
        }
    }

    changePassword(oldPassword: string, newPassword: string) {
        oldPassword = hash(oldPassword);
        newPassword = hash(newPassword);
        let url = ConfigService.buildUrl("update_user_change_password.php");
        let body = JSON.stringify({oldPassword, newPassword});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers});
        console.info(body);
        return this.http.post(url,body,options);
    }
}