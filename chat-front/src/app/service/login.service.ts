import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {ConfigService} from '../config.service';



// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ChatService} from "./chat.service";
import {Router} from "@angular/router";
import {hash} from "../util/sha1";


@Injectable()
export class LoginService{

    public static getLoggedKey(): string { return 'X-Auth-Token'; }
    public static getAdminKey():string { return "admin"; }

    constructor(private http:Http, private chatService:ChatService, private router:Router){}

    loginAnonym(nick:string,city:string,birthday, sex:number):Observable<Response>{
        let url = ConfigService.buildUrl("login_anonym.php");
        let body = JSON.stringify({nick,city,birthday, sex});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers});
        return this.http.post(url,body,options);
    }

    loginUser(login: string, password: string):Observable<Response> {
        password =  hash(password);
        console.info(password + " ");

        let body = JSON.stringify({ login, password });
        let loginUrl = ConfigService.buildUrl('login_user.php');
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });
        return this.http.post(loginUrl,body,options);
    }

    onLogged():void{
        localStorage.setItem(LoginService.getLoggedKey(), "1");
        this.chatService.startChecking();
    }

    isLogged():boolean {
       return localStorage.getItem(LoginService.getLoggedKey()) !=null;
    }

    logout():Observable<Response> {
        let url = ConfigService.buildUrl("logout.php");
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers});
        return this.http.post(url,{},options);
    }

    onLogout(){
        this.chatService.stopChecking();
        localStorage.removeItem(LoginService.getLoggedKey());
        localStorage.removeItem(LoginService.getAdminKey());
        this.router.navigate([""]);
    }
}