import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {ConfigService} from '../config.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {hash} from "../util/sha1";

@Injectable()
export class RegisterService{

    constructor(private http: Http) { }


    register(nick, email, password,city, birthday, sex): Observable<Response> {
        password =  hash(password);
        let body = JSON.stringify({ nick ,email, password,city, birthday, sex });
        let url = ConfigService.buildUrl('register.php');
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url,body,options);
    }

    activeUser(token:string):Observable<Response>{
        let url = ConfigService.buildUrl('activation.php?token=') + token;
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers});

        return this.http.post(url, {}, options);
    }
/*
    sendRecoverPasswordMail(login:String):Observable<Response> {
        let url = ConfigService.buildUrl(RegisterService.path + '/recover?login=' + login);  // URL to web api
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url,{},options);
    }

    recover(password:String, token:string):Observable<Response> {
        let body = JSON.stringify({password, token});
        let url = ConfigService.buildUrl(RegisterService.path + '/passwordRecover');
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url,body,options);
    }*/
}