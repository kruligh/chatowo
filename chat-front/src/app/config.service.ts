import {Injectable} from '@angular/core';

@Injectable()
export class ConfigService{

/*        private  static  backendUrl = "http://164.132.57.18/~chatowo/server";*/
     //private static backendUrl = "http://chatowe.gear.host/server";
    private  static  backendUrl = "http://localhost/server";
    public static getBaseUrl():string {
        return ConfigService.backendUrl;
    }

    public static buildUrl(path:string) : string {
        return ConfigService.backendUrl + "/" + path;
    }


}