import {User} from "./user.model";

export class Report{
    private _id:number;
    private _user:User;
    private _messageContent:string;
    private _reason:string;


    constructor(id: number, user: User, messageContent: string, reason: string) {
        this._id = id;
        this._user = user;
        this._messageContent = messageContent;
        this._reason = reason;
    }


    get id(): number {
        return this._id;
    }

    get user(): User {
        return this._user;
    }

    get messageContent(): string {
        return this._messageContent;
    }

    get reason(): string {
        return this._reason;
    }
}