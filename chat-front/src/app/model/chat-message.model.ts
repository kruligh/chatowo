import {User} from "./user.model";
import {JsonPipe} from "@angular/common";

export class ChatMessage{

    private _id:number;
    private _content:string;
    private _user:User;
    private _sendDate;

    constructor(id: number, content: string, user: User, sendDate) {
        this._id = id;
        this._content = content;
        this._user = user;
        this._sendDate = sendDate;
    }


    get id(): number {
        return this._id;
    }

    get content(): string {
        return this._content;
    }

    get user(): User {
        return this._user;
    }

    get sendDate() {
        return this._sendDate;
    }
}