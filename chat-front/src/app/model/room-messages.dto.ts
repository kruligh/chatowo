import {ChatMessage} from "./chat-message.model";

export class RoomMessagesDto{
    private _roomId:number;
    private _privUserId:number;
    private _messages: ChatMessage[];

    constructor(roomId, userId, messages: ChatMessage[]) {
        this._roomId = parseInt(roomId);
        this._privUserId = parseInt(userId);
        this._messages = messages;
    }

    get roomId(): number {
        return this._roomId;
    }


    get privUserId(): number {
        return this._privUserId;
    }

    get messages(): ChatMessage[] {
        return this._messages;
    }
}