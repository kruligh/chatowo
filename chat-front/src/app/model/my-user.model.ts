export class MyUser{
    private _id;
    private _userType;
    private _nick;
    private _sex;
    private _email;
    private _status;
    private _city;
    private _age;
    private _birthday;
    private _active;


    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get userType() {
        return this._userType;
    }

    set userType(value) {
        this._userType = value;
    }

    get nick() {
        return this._nick;
    }

    set nick(value) {
        this._nick = value;
    }

    get sex() {
        return this._sex;
    }

    set sex(value) {
        this._sex = value;
    }

    get email() {
        return this._email;
    }

    set email(value) {
        this._email = value;
    }

    get status() {
        return this._status;
    }

    set status(value) {
        this._status = value;
    }

    get city() {
        return this._city;
    }

    set city(value) {
        this._city = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age= value;
    }

    get birthday() {
        return this._birthday;
    }

    set birthday(value) {
        this._birthday = value;
    }

    get active() {
        return this._active;
    }

    set active(value) {
        this._active = value;
    }
}