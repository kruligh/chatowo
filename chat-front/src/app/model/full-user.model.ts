export class FullUser {
    private _id;
    private _userType;
    private _nick;
    private _sex;
    private _status;
    private _city;
    private _age;


    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get userType() {
        return this._userType;
    }

    set userType(value) {
        this._userType = value;
    }

    get nick() {
        return this._nick;
    }

    set nick(value) {
        this._nick = value;
    }

    get sex() {
        return this._sex;
    }

    set sex(value) {
        this._sex = value;
    }

    get status() {
        return this._status;
    }

    set status(value) {
        this._status = value;
    }

    get city() {
        return this._city;
    }

    set city(value) {
        this._city = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }
}