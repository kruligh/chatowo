export class Room{
    private _id:number;
    private _name:string;
    private _usersCount:number;


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }


    get usersCount(): number {
        return this._usersCount;
    }

    set usersCount(value: number) {
        this._usersCount = value;
    }
}