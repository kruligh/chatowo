export class Conversation{
    private _id:number;
    private _roomId:number;
    private _privUserId:number;
    private _name:string;
    private _newMessageCount:number;

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get roomId(): number {
        return this._roomId;
    }

    set roomId(value: number) {
        this._roomId = value;
    }

    get privUserId(): number {
        return this._privUserId;
    }

    set privUserId(value: number) {
        this._privUserId = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get newMessageCount(): number {
        return this._newMessageCount;
    }

    set newMessagesCount(newMsg: number) {
        this._newMessageCount = newMsg;
    }

}