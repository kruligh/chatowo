export class User{
    private _id:number;
    private _userType:number;
    private _nick:string;


    constructor(id: number, userType: number, nick: string) {
        this._id = id;
        this._userType = userType;
        this._nick = nick;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get nick(): string {
        return this._nick;
    }

    set nick(value: string) {
        this._nick = value;
    }

    get userType(): number {
        return this._userType;
    }

    set userType(value: number) {
        this._userType = value;
    }
}