import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {UserService} from "../service/user.service";

@Injectable()
export class IsAdminGuard implements CanActivate {
    constructor(private router : Router) {}

    canActivate() {
        if(UserService.isAdmin()){
            return true;
        }else{
            this.router.navigate(['chat']);
            return false;
        }
    }

}