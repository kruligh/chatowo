import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LoginService } from '../service/login.service';

@Injectable()
export class LoggedInGuard implements CanActivate {
    constructor(private loginService: LoginService, private router : Router) {}

    canActivate() {
        if(this.loginService.isLogged()){
            return true;
        }else{
            this.router.navigate(['']);
            return false;
        }
    }

}
