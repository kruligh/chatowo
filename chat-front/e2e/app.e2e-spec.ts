import { ChatFrontPage } from './app.po';

describe('chat-front App', function() {
  let page: ChatFrontPage;

  beforeEach(() => {
    page = new ChatFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
